<?php
	session_start();
	if (!isset($_SESSION['usrLogeado']))
		header ('location: index.php');
	unset( $_SESSION['usrLogeado'] );
	session_destroy();
	header( 'location: index.php' );
?>