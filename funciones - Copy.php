<?php
	error_reporting( E_ALL & ~( E_STRICT | E_NOTICE | E_DEPRECATED ) );
	define( 'AMBIENTE', 'PRODUCCION' );
//	define( 'AMBIENTE', 'PRUEBAS' );
		//if( AMBIENTE == 'PRUEBAS' )
	session_start();
	include ( 'db.php' );
	define( 'REGISTROS_X_PAGINA', 3000 );
	define( 'DEBBUGER', true);
	define( 'IMAGENES_ALTO', 50);
	define( 'IMAGENES_ANCHO', 50);
	define( 'ICONOS_ALTO', 25);
	define( 'ICONOS_ANCHO', 25);
	define( 'SALIDA_SERVICIOS', 'json' );
	define( 'DS', DIRECTORY_SEPARATOR );
	date_default_timezone_set('America/Los_Angeles');
		if ( AMBIENTE == 'PRODUCCION' ){
			// PRODUCCION
			//$cnx = array ( "server"=>"localhost", "usuario"=>"root", "pass"=>"root", "nombreDb"=>"inventarios" );
            //$cnx = array ( "server"=>"localhost", "usuario"=>"myhxmx_INVENTARIOS", "pass"=>"zaq12wsx", "nombreDb"=>"myhxmx_INVENTARIOS" );
	$cnx = array ( "server"=>"localhost", "usuario"=>"root", "pass"=>"", "nombreDb"=>"Inventarios" );
		}else{
			// DESARROLLO CaRLoS
			//$cnx = array ( "server"=>"localhost", "usuario"=>"root", "pass"=>"mysql", "nombreDb"=>"inventarios" );
			// DESARROLLO oOMovil
			$cnx = array ( "server"=>"localhost:8889", "usuario"=>"root", "pass"=>"root", "nombreDb"=>"inventariosHersheys" );
		}

	function array2xml(array $arr, SimpleXMLElement $xml){
			foreach ($arr as $k => $v) {
				is_array($v)
					? array2xml($v, $xml->addChild( ( is_numeric($k) ? "r$k" : $k ) ))
					: $xml->addChild($k, $v);
			}
		return $xml;
	}

	function formatData( $data, $format='json' ){
			if ( $format == 'json' ){
				header( 'Content-type: application/json' );
				return json_encode( $data );
			}else{
				header( 'Content-type: text/xml' );
				return array2xml($data, new SimpleXMLElement('<respuesta/>'))->asXML();
			}
	}

	function parametro ( $parametro ){
		return ( isset( $_POST[$parametro] ) ? utf8_decode($_POST[$parametro]) : ( isset( $_GET[$parametro] ) ? utf8_decode($_GET[$parametro]) : '' ) );
	}

	function getConexion($conexion){
		return new db( db::MYSQL, $conexion["server"], $conexion["usuario"], $conexion["pass"], $conexion["nombreDb"] );
	}

	function error( $error ){
		if( DEBBUGER ) die($error); else die();
	}

	function logea ( $login, $pass, $usuarioTablet=false ){
		global $cnx;
			if( $login == 'Administrador' && $pass == 'AdmonInventarios' )
				return true;
			else{
				$db = getConexion( $cnx );
				//$db = new db( db::MYSQL, $cnx["server"], $cnx["usuario"], $cnx["pass"], $cnx["nombreDb"] );
					if( $db->conecta() ){
						$query = "select login, pass from usuarios where login = '$login'" . ( $usuarioTablet == false ? " and usrTablet = 0" : "" );
						$link = $db->db_query( $query ) or die ( "Error en la consulta: $query" );
						$datos = $db->db_fetch_object( $link );
							if(isset($datos->login) and $datos->pass == $pass){
					 			return true;
							}
					}
			}
		return false;
	}

	function getNombreUsuario($login){
		global $cnx;
		$nombre = "";
		$db = getConexion($cnx);
			if( $db->conecta() ){
				$query = "select nombre from usuarios where login = '$login'";
				$link = $db->db_query($query) or error("Error en la consulta: $query");
				$datos = $db->db_fetch_object($link);
				$nombre = $datos->nombre;
			}else 
				$nombre = "Error en conexion.";
		return $nombre;
	}

	function getNombreEvento($idEvento){
		global $cnx;
		$nombre = "";
		$db = getConexion($cnx);
			if($db->conecta()){
				$query = "select nombre, date_format( fechaInicial, '%d/%m/%Y' ) fechaInicial, date_format( fechaFinal, '%d/%m/%Y' ) fechaFinal from eventos where id = $idEvento";
				$link = $db->db_query($query) or error("Error en la consulta: $query");
				$datos = $db->db_fetch_object($link);
					if(isset($datos->nombre)){
						$nombre = "{$datos->nombre} con fechas del {$datos->fechaInicial} al {$datos->fechaFinal}";
					}
			}
		return $nombre;
	}

	function ParseaCVS($linea){
		//echo "cadena original: $linea<br>"; 
		$cont = 0;
			while((strpos($linea, '"') !== false) && ($cont < 55)){
				//echo "Linea: $linea<br>";
				$izq = substr($linea, 0, strpos($linea, '"'));
				//echo "izq: $izq<br>";
				$fix = substr($linea, strpos($linea, '"'));
				//echo "fix: $fix<br>";
				$posSegundaComa = strpos($fix, '"', 1) + 1;
				$fix = substr($fix, 0, $posSegundaComa);
				//echo "solo fix: $fix<br>";
				$der = substr($linea, strlen($izq) + $posSegundaComa);
				$fix = str_replace(',', 'º', $fix);
				$fix = str_replace('"', 'ª', $fix);
				$linea = $izq . $fix . $der;
				//echo "linea pasada $cont: $linea<br>";
				$cont++;
			}
		$linea = str_replace('ª', '"', $linea);
		//echo "cadena fix: $linea<br>";
		return $linea;	
	}
	
	function ArreglaCampo($campo){ return utf8_encode(str_replace('"', '', str_replace("'", "\'", str_replace('º', ',', $campo)))); }
	
	function EjecutaScript($script){
		global $cnx;
		$db = getConexion($cnx);
			if( $db->conecta())
				$db->db_query($script) or error("Error en el script: $script, " . mysql_error() );
	}
	
	function getInventario($pagina = "0", $buscar, $colBuscar, $colOrdenar = "", $ordDireccion = "asc"){
		$colOrdenar = ( $colOrdenar == "" ? "respCostCenter asc, asset asc, subnumber " : $colOrdenar );
		$ordDireccion = ( $ordDireccion == "" ? "asc" : $ordDireccion );
		global $cnx;
		$t = "<div style='width:100%; text-align:left'>
					<a name='Resultados'></a>
					<input class='btnFiltro' type='button' value='Filtro' onclick=\"mostrarFiltro('filtro');\"/>
					<div class='filtro' id='filtro' style='display:none'>
						&nbsp;
						<p>
						Buscar: <input type='text' value='$buscar' id='buscar'/> 
						en: <select id='sColBuscar'>
								<option value='asset' " . ( $colBuscar == "asset" ? "selected" : "") . ">Asset</option>
								<option value='subnumber' " . ( $colBuscar == "subnumber" ? "selected" : "") . ">Subnumber</option>
								<option value='capitalizedOn' " . ( $colBuscar == "capitalizedOn" ? "selected" : "") . ">Capitalized On</option>
								<option value='description' " . ( $colBuscar == "description" ? "selected" : "") . ">Description</option>" .
								/*
								<option value='acquisVal' " . ( $colBuscar == "acquisVal" ? "selected" : "") . ">Acquis. Val</option>
								<option value='accumDep' " . ( $colBuscar == "accumDep" ? "selected" : "") . ">Accum. Dep</option>
								*/
								"<option value='bookVal' " . ( $colBuscar == "bookVal" ? "selected" : "") . ">Book Val</option>
								<option value='currency' " . ( $colBuscar == "currency" ? "selected" : "") . ">Currency</option>
								<option value='plant' " . ( $colBuscar == "plant" ? "selected" : "") . ">Plant</option>
								<option value='respCostCenter' " . ( $colBuscar == "respCostCenter" ? "selected" : "") . ">Resp. Cost Center</option>
								<option value='pm' " . ( $colBuscar == "pm" ? "selected" : "") . ">PM</option>
								<option value='order' " . ( $colBuscar == "order" ? "selected" : "") . ">Order</option>
								<option value='expiredUseFulLife' " . ( $colBuscar == "expiredUseFulLife" ? "selected" : "") . ">Expired useful life</option>
								<option value='ubicacion' " . ( $colBuscar == "ubicacion" ? "selected" : "") . ">Ubicación</option>
								<option value='marca' " . ( $colBuscar == "marca" ? "selected" : "") . ">Marca</option>
								<option value='modelo' " . ( $colBuscar == "modelo" ? "selected" : "") . ">Modelo</option>
								<option value='noSerie' " . ( $colBuscar == "noSerie" ? "selected" : "") . ">noSerie</option>
								<option value='pedimento' " . ( $colBuscar == "pedimento" ? "selected" : "") . ">Pedimento</option>
								<option value='statusEtiquetado' " . ( $colBuscar == "statusEtiquetado" ? "selected" : "") . ">Status de etiquetado</option>
								<option value='statusActivo' " . ( $colBuscar == "statusActivo" ? "selected" : "") . ">Status del Activo</option>
								<option value='origen' " . ( $colBuscar == "origen" ? "selected" : "") . ">Origen</option>
								<option value='comentario' " . ( $colBuscar == "comentario" ? "selected" : "") . ">Comentario</option>
							</select>
						ordenar: <select id='sColOrdenar'>
								<option value='' >Default</option>
								<option value='asset' " . ( $colOrdenar == "asset" ? "selected" : "") . ">Asset</option>
								<option value='subnumber' " . ( $colOrdenar == "subnumber" ? "selected" : "") . ">Subnumber</option>
								<option value='capitalizedOn' " . ( $colOrdenar == "capitalizedOn" ? "selected" : "") . ">Capitalized On</option>
								<option value='description' " . ( $colOrdenar == "description" ? "selected" : "") . ">Description</option>" .
								/*
								<option value='acquisVal' " . ( $colOrdenar == "acquisVal" ? "selected" : "") . ">Acquis. Val</option>
								<option value='accumDep' " . ( $colOrdenar == "accumDep" ? "selected" : "") . ">Accum. Dep</option>
								*/
								"<option value='bookVal' " . ( $colOrdenar == "bookVal" ? "selected" : "") . ">Book Val</option>
								<option value='currency' " . ( $colOrdenar == "currency" ? "selected" : "") . ">Currency</option>
								<option value='plant' " . ( $colOrdenar == "plant" ? "selected" : "") . ">Plant</option>
								<option value='respCostCenter' " . ( $colOrdenar == "respCostCenter" ? "selected" : "") . ">Resp. Cost Center</option>
								<option value='pm' " . ( $colBuscar == "pm" ? "selected" : "") . ">PM</option>
								<option value='order' " . ( $colOrdenar == "order" ? "selected" : "") . ">Order</option>
								<option value='expiredUseFulLife' " . ( $colOrdenar == "expiredUseFulLife" ? "selected" : "") . ">Expired useful life</option>
								<option value='ubicacion' " . ( $colOrdenar == "ubicacion" ? "selected" : "") . ">Ubicación</option>
								<option value='marca' " . ( $colOrdenar == "marca" ? "selected" : "") . ">Marca</option>
								<option value='modelo' " . ( $colOrdenar == "modelo" ? "selected" : "") . ">Modelo</option>
								<option value='noSerie' " . ( $colOrdenar == "noSerie" ? "selected" : "") . ">noSerie</option>
								<option value='pedimento' " . ( $colOrdenar == "pedimento" ? "selected" : "") . ">Pedimento</option>
								<option value='statusEtiquetado' " . ( $colOrdenar == "statusEtiquetado" ? "selected" : "") . ">Status de etiquetado</option>
								<option value='statusActivo' " . ( $colOrdenar == "statusActivo" ? "selected" : "") . ">Status del Activo</option>
								<option value='origen' " . ( $colOrdenar == "origen" ? "selected" : "") . ">Origen</option>
								<option value='comentario' " . ( $colOrdenar == "comentario" ? "selected" : "") . ">Comentario</option>
							</select>
						direccion: <select id='sOrdeDireccion'>
									<option value='asc' " . ( $ordDireccion == "asc" ? "selected" : "") . ">Ascendente</option>
									<option value='desc' " . ( $ordDireccion == "desc" ? "selected" : "") . ">Descendente</option>
								</select>
						<input type='button' value='Filtrar' onclick='getInventario(0);' />	
						</p>
						<br />
					</div>
				</div>   
				%listadoPaginas%                 
				<table class='tablaDatos' border='1' cellpadding='0' cellspacing='0'>
                    	<tr class='rowTitulo'>
                        	<td><a onclick='addInventario();'><img src='img/add.png' /></a></td>
                            <td>Fotografía</td>
                            <td>Asset</td>
                            <td>Subnumber</td>
                            <td>Capitalized<br />On</td>
                            <td>Description</td>" .
							/*
                            <td>Acquis. Val</td>
                            <td>Accum. Dep</td>
							*/
                            "<td>Book Val</td>
                            <td>Currency</td>
                            <td>Plant</td>
                            <td>Resp.<br />cost.<br />Center</td>
							<td>PM</td>
                            <td>Order</td>
                            <td>Expired<br />useful<br />life</td>
                            <td>Ubicación</td>
                            <td>Marca</td>
                            <td>Modelo</td>
                            <td>No.<br />Serie</td>
                            <td>Pedimento</td>
                            <td>Status <br />de<br />etiquetado</td>
                            <td>Status <br />del<br />activo</td>
                            <td>Origen</td>
                            <td>Comentarios</td>
                        </tr>";
			$db = getConexion($cnx);
				if($db->conecta()){
                    setlocale (LC_TIME, "es_MX");

                    // MODIFICIACION KUFF
                    $trans = array(
                        'Monday'    => 'Lunes',
                        'Tuesday'   => 'Martes',
                        'Wednesday' => 'Miercoles',
                        'Thursday'  => 'Jueves',
                        'Friday'    => 'Viernes',
                        'Saturday'  => 'Sabado',
                        'Sunday'    => 'Domingo',
                        'Mon'       => 'Mo',
                        'Tue'       => 'Di',
                        'Wed'       => 'Mi',
                        'Thu'       => 'Do',
                        'Fri'       => 'Fr',
                        'Sat'       => 'Sa',
                        'Sun'       => 'So',
                        'January'   => 'Enero',
                        'February'  => 'Febrero',
                        'March'     => 'Marzo',
                        'May'       => 'Mayo',
                        'June'      => 'Junio',
                        'July'      => 'Julio',
                        'October'   => 'Octubre',
                        'November'   => 'Noviembre',
                        'December'  => 'Diciembre',
                        'Mar'       => 'M&auml;r',
                        'Oct'       => 'Okt',
                        'Dec'       => 'Dez',
                    );

					//$pagina--;
					$colOrdenar = ( $colOrdenar == 'order' ? '`order`' : $colOrdenar );
                    //MODIFICACION KUFF
					$query = "select *, date_format( capitalizedOn, '%d de %M del %Y' ) capitalizedOn2  from inventario" .
							( $buscar != "" ? " where $colBuscar like '%$buscar%'" : '' ) .
							( $colOrdenar != "" ? " order by $colOrdenar $ordDireccion" : "" ) .
							" limit " . ( $pagina * REGISTROS_X_PAGINA ) . ", " . REGISTROS_X_PAGINA;
					$link = $db->db_query($query) or error("Error en la consulta: $query");
						while($datos = $db->db_fetch_object($link)){
							$imagen = $datos->asset . '.jpg';
							if(!file_exists('imgInventario/' . $imagen))
								$imagen = "no-image.png";
							$t .= "<tr>
									<td><a onclick='borrar({$datos->id})'><img src='img/delete.png' heigth='" . ICONOS_ALTO . "' width='" . ICONOS_ANCHO . "' /></a>
										&nbsp;<a onclick='editar({$datos->id})'><img src='img/editar.png' heigth='" . ICONOS_ALTO . "' width='" . ICONOS_ANCHO . "'/></a>" .
										( $datos->subnumber == 0 
											? "&nbsp;<a onclick='editarFoto(\"{$datos->asset}\")'><img src='img/editarFoto.png' heigth='" . ICONOS_ALTO . "' width='" . ICONOS_ANCHO . "'/></a>"
											: "" ) . "
									</td>
									<td>" . ( $datos->subnumber == 0 
												? "<a onclick=\"verFoto('$imagen')\"><img src='imgInventario/$imagen' wwidth='" . IMAGENES_ANCHO . "' height='" . IMAGENES_ALTO . "'/></a>"
												: "<img src='imgInventario/no-image.png' wwidth='" . IMAGENES_ANCHO . "' height='" . IMAGENES_ALTO . "'/>" ) . 
									"</td>	
									<td>{$datos->asset}</td>
									<td>{$datos->subnumber}</td>
									<td>".strtr($datos->capitalizedOn2, $trans).//MODIFICACION KUFF
									"</td>
									<td>{$datos->description}</td>".
									/*
									<td>" . number_format($datos->acquisVal, 2) . "</td>
									<td>" . number_format($datos->accumDep, 2) . "</td>
									*/
									"<td>" . number_format($datos->bookVal, 2) . "</td>
									<td>{$datos->currency}</td>
									<td>{$datos->plant}</td>
									<td>{$datos->respCostCenter}</td>
									<td>{$datos->pm}</td>
									<td>{$datos->order}</td>
									<td>{$datos->expiredUsefulLife}</td>
									<td>{$datos->ubicacion}</td>
									<td>{$datos->marca}</td>
									<td>{$datos->modelo}</td>
									<td>{$datos->noSerie}</td>
									<td>{$datos->pedimento}</td>
									<td>{$datos->statusEtiquetado}</td>
									<td>{$datos->statusActivo}</td>
									<td>{$datos->origen}</td>
									<td>{$datos->comentarios}</td>
								</tr>";
						}
					$query = "select count(id) noRegistros from inventario" . 
							( $buscar != "" ? " where $colBuscar like '%$buscar%'" : '' ) .
							( $colOrdenar != "" ? " order by $colOrdenar $ordDireccion" : "" );
					$link = $db->db_query( $query ) or error("Error en la consulta: $query");
					$datos = $db->db_fetch_object($link);
					$total = ceil ( $datos->noRegistros / REGISTROS_X_PAGINA );
					$listaPaginas = "";
						for($x = 0; $x < $total && $total > 1; $x++){
								if($pagina == $x){
									//$listaPaginas .= "<span class='pag_actual'>$x</span>";
									$listaPaginas .= "<option value='$x' selected='selected'>$x</option>";
								}else{
									//$listaPaginas .= "<a href='Resultados' class='pag_selecionar' onclick=\"getInventario($x)\">$x</a>";
									$listaPaginas .= "<option value='$x'>$x</option>";
								}
						}
					$listaPaginas = ( $listaPaginas != "" ? "Página:<select id='sPagina' class='filtro' onchange=\"getInventario(this.value)\">" . $listaPaginas . "</select>&nbsp;" : "" );
					$listaPaginas = "<div width='100%' align='center'>" . $listaPaginas . "Registros:" . number_format($datos->noRegistros,0) . "</div>";
					$t = str_replace("%listadoPaginas%", $listaPaginas, $t);
				}
			$t .= "</table>";
			return $t;
	}
	
	function getUsuarios($pagina = "0", $buscar, $colBuscar, $colOrdenar = "", $ordDireccion = ""){
		$colOrdenar = ( $colOrdenar == "" ? "" : $colOrdenar );
		$ordDireccion = ( $ordDireccion == "" ? "" : $ordDireccion );
		global $cnx;
		$t = "<div style='width:100%; text-align:left'>
				<a name='Resultados'></a>
				%listadoPaginas%                 
				<table class='tablaDatos' border='1' cellpadding='0' cellspacing='0'>
                    	<tr class='rowTitulo'>
                        	<td><a onclick='addUsuario();'><img src='img/add.png' /></a></td>
                            <td>Login</td>
                            <td>Nombre</td>
                            <td>email</td>
                            <td>Solo usuario tablet</td>
                        </tr>";
			$db = getConexion($cnx);
				if($db->conecta()){
					//$pagina--;
					//$colOrdenar = ( $colOrdenar == 'order' ? '`order`' : $colOrdenar );
					$query = "select * from usuarios" . 
							( $buscar != "" ? " where $colBuscar like '%$buscar%'" : '' ) .
							( $colOrdenar != "" ? " order by $colOrdenar $ordDireccion" : "" ) .
							" limit " . ( $pagina * REGISTROS_X_PAGINA ) . ", " . REGISTROS_X_PAGINA;
					$link = $db->db_query($query) or error("Error en la consulta: $query");
						while($datos = $db->db_fetch_object($link)){
							$t .= "<tr>
									<td><a onclick=\"javascript:borrarUsuario('{$datos->login}')\"><img src='img/delete.png' heigth='" . ICONOS_ALTO . "' width='" . ICONOS_ANCHO . "'/></a>
										&nbsp;
										<a onclick=\"javascript:editarUsuario('{$datos->login}')\"><img src='img/editar.png' heigth='" . ICONOS_ALTO . "' width='" . ICONOS_ANCHO . "'/></a>
									</td>
									<td>{$datos->login}</td>
									<td>{$datos->nombre}</td>
									<td>{$datos->email}</td>
									<td>" . ( $datos->usrTablet ? "Si" : "No" ) . "</td>
								</tr>";
						}
					$query = "select count(login) noRegistros from usuarios" . 
							( $buscar != "" ? " where $colBuscar like '%$buscar%'" : '' ) .
							( $colOrdenar != "" ? " order by $colOrdenar $ordDireccion" : "" );
					$link = $db->db_query( $query ) or error("Error en la consulta: $query");
					$datos = $db->db_fetch_object($link);
					$total = ceil ( $datos->noRegistros / REGISTROS_X_PAGINA );
					$listaPaginas = "";
						for($x = 0; $x < $total && $total > 1; $x++){
								if($pagina == $x){
									//$listaPaginas .= "<span class='pag_actual'>$x</span>";
									$listaPaginas .= "<option value='$x' selected='selected'>$x</option>";
								}else{
									//$listaPaginas .= "<a href='Resultados' class='pag_selecionar' onclick=\"getInventario($x)\">$x</a>";
									$listaPaginas .= "<option value='$x'>$x</option>";
								}
						}
					$listaPaginas = ( $listaPaginas != "" ? "Página:<select id='sPagina' class='filtro' onchange=\"getUsuarios(this.value)\">" . $listaPaginas . "</select>&nbsp;" : "" );
					$listaPaginas = "<div width='100%' align='center'>" . $listaPaginas . "Registros:" . number_format($datos->noRegistros,0) . "</div>";
					$t = str_replace("%listadoPaginas%", $listaPaginas, $t);
				}
			$t .= "</table>";
			return $t;
	}

	function getEventos($pagina = "0", $buscar, $colBuscar, $colOrdenar = "", $ordDireccion = ""){
		$colOrdenar = ( $colOrdenar == "" ? "" : $colOrdenar );
		$ordDireccion = ( $ordDireccion == "" ? "" : $ordDireccion );
		global $cnx;
		$t = "<div style='width:100%; text-align:left'>
				<a name='Resultados'></a>
				%listadoPaginas%                 
				<table class='tablaDatos' border='1' cellpadding='0' cellspacing='0'>
                    	<tr class='rowTitulo'>
                        	<td><a onclick='addEvento();'><img src='img/add.png' /></a></td>
                            <td>Nombre</td>
                            <td>Fecha Inicial</td>
                            <td>Fecha Final</td>
							<td>Avance</td>
                            <td>Participantes</td>
                        </tr>";
			$db = getConexion($cnx);
				if($db->conecta()){
					//$pagina--;
					//$colOrdenar = ( $colOrdenar == 'order' ? '`order`' : $colOrdenar );
					$query = "select *, date_format( fechaInicial, '%d/%m/%Y' ) fechaInicial2, date_format( fechaFinal, '%d/%m/%Y' ) fechaFinal2,
								( select count(*) from eventosInventario where eventos_id = eventos.id and origenRegistro = 'Inventario' and subnumber = '0' ) inventario,
								( select count(*) from eventosInventario where eventos_id = eventos.id and origenRegistro = 'Tablet' and subnumber = '0' ) tablet 
								from eventos" . 
							( $buscar != "" ? " where $colBuscar like '%$buscar%'" : '' ) .
							( $colOrdenar != "" ? " order by $colOrdenar $ordDireccion" : "" ) .
							" limit " . ( $pagina * REGISTROS_X_PAGINA ) . ", " . REGISTROS_X_PAGINA;
					$link = $db->db_query($query) or error("Error en la consulta: $query");
						while($datos = $db->db_fetch_object($link)){
							$participantes = "";
							$query = "select usuarios_login from eventosParticipantes where eventos_id = {$datos->id}";
							$link2 = $db->db_query($query) or error("Error en la consulta: $query");
								while($datos2 = $db->db_fetch_object($link2)){
									$participantes .= ( $participantes != "" ? "," : "" ) . "{$datos2->usuarios_login}";
								}
							$t .= "<tr>
									<td><a onclick=\"borrarEvento({$datos->id})\"><img src='img/delete.png' heigth='" . ICONOS_ALTO . "' width='" . ICONOS_ANCHO . "'/></a>
										&nbsp;
										<a onclick=\"editarEvento({$datos->id})\"><img src='img/editar.png' heigth='" . ICONOS_ALTO . "' width='" . ICONOS_ANCHO . "' /></a>
										&nbsp;
										<a href='detalleInventario.php?idEvento={$datos->id}'><img src='img/iconoInventario.png' heigth='" . ICONOS_ALTO . "' width='" . ICONOS_ANCHO . "' /></a> 
									</td>
									<td>{$datos->nombre}</td>
									<td>{$datos->fechaInicial2}</td>
									<td>{$datos->fechaFinal2}</td>
									<td>{$datos->tablet} de {$datos->inventario} [" . ( $datos->inventario > 0 ? number_format((($datos->tablet/$datos->inventario)*100), 4) : '0.0000' ) . "%]</td>
									<td>$participantes</td>
								</tr>";
						}
					$query = "select count(id) noRegistros from eventos" . 
							( $buscar != "" ? " where $colBuscar like '%$buscar%'" : '' ) .
							( $colOrdenar != "" ? " order by $colOrdenar $ordDireccion" : "" );
					$link = $db->db_query( $query ) or error("Error en la consulta: $query");
					$datos = $db->db_fetch_object($link);
					$total = ceil ( $datos->noRegistros / REGISTROS_X_PAGINA );
					$listaPaginas = "";
						for($x = 0; $x < $total && $total > 1; $x++){
								if($pagina == $x){
									//$listaPaginas .= "<span class='pag_actual'>$x</span>";
									$listaPaginas .= "<option value='$x' selected='selected'>$x</option>";
								}else{
									//$listaPaginas .= "<a href='Resultados' class='pag_selecionar' onclick=\"getInventario($x)\">$x</a>";
									$listaPaginas .= "<option value='$x'>$x</option>";
								}
						}
					$listaPaginas = ( $listaPaginas != "" ? "Página:<select id='sPagina' class='filtro' onchange=\"getEventos(this.value)\">" . $listaPaginas . "</select>&nbsp;" : "" );
					$listaPaginas = "<div width='100%' align='center'>" . $listaPaginas . "Registros:" . number_format($datos->noRegistros,0) . "</div>";
					$t = str_replace("%listadoPaginas%", $listaPaginas, $t);
				}
			$t .= "</table>";
			return $t;
	}

	function getEventoInventario($idEvento, $pagina = "0", $buscar, $colBuscar, $colOrdenar = "", $ordDireccion = "asc"){
		$colOrdenar = ( $colOrdenar == "" ? "respCostCenter asc, asset asc, origenRegistro desc, subnumber " : $colOrdenar );
		$ordDireccion = ( $ordDireccion == "" ? "asc" : $ordDireccion );
		global $cnx;
		$t = "<div style='width:100%; text-align:left'>
					<a name='Resultados'></a>
					<input class='btnFiltro' type='button' value='Filtro y Reporte' onclick=\"mostrarFiltro('filtro2');\"/>
					<div class='filtro' id='filtro2' style='display:none;' >
						&nbsp;
						<p>
							Buscar: <input type='text' value='$buscar' id='buscar'/> 
							en: <select id='sColBuscar'>
									<option value='origenRegistro' " . ( $colBuscar == "origenRegistro" ? "selected" : "") . ">Tipo</option>
									<option value='asset' " . ( $colBuscar == "asset" ? "selected" : "") . ">Asset</option>
									<option value='subnumber' " . ( $colBuscar == "subnumber" ? "selected" : "") . ">Subnumber</option>
									<option value='capitalizedOn' " . ( $colBuscar == "capitalizedOn" ? "selected" : "") . ">Capitalized On</option>
									<option value='description' " . ( $colBuscar == "description" ? "selected" : "") . ">Description</option>" .
									/*
									<option value='acquisVal' " . ( $colBuscar == "acquisVal" ? "selected" : "") . ">Acquis. Val</option>
									<option value='accumDep' " . ( $colBuscar == "accumDep" ? "selected" : "") . ">Accum. Dep</option>
									*/
									"<option value='bookVal' " . ( $colBuscar == "bookVal" ? "selected" : "") . ">Book Val</option>
									<option value='currency' " . ( $colBuscar == "currency" ? "selected" : "") . ">Currency</option>
									<option value='plant' " . ( $colBuscar == "plant" ? "selected" : "") . ">Plant</option>
									<option value='respCostCenter' " . ( $colBuscar == "respCostCenter" ? "selected" : "") . ">Resp. Cost Center</option>
									<option value='pm' " . ( $colBuscar == "pm" ? "selected" : "") . ">PM</option>
									<option value='order' " . ( $colBuscar == "order" ? "selected" : "") . ">Order</option>
									<option value='expiredUseFulLife' " . ( $colBuscar == "expiredUseFulLife" ? "selected" : "") . ">Expired useful life</option>
									<option value='ubicacion' " . ( $colBuscar == "ubicacion" ? "selected" : "") . ">Ubicación</option>
									<option value='marca' " . ( $colBuscar == "marca" ? "selected" : "") . ">Marca</option>
									<option value='modelo' " . ( $colBuscar == "modelo" ? "selected" : "") . ">Modelo</option>
									<option value='noSerie' " . ( $colBuscar == "noSerie" ? "selected" : "") . ">noSerie</option>
									<option value='pedimento' " . ( $colBuscar == "pedimento" ? "selected" : "") . ">Pedimento</option>
									<option value='statusEtiquetado' " . ( $colBuscar == "statusEtiquetado" ? "selected" : "") . ">Status de etiquetado</option>
									<option value='statusActivo' " . ( $colBuscar == "statusActivo" ? "selected" : "") . ">Status del Activo</option>
									<option value='origen' " . ( $colBuscar == "origen" ? "selected" : "") . ">Origen</option>
									<option value='comentario' " . ( $colBuscar == "comentario" ? "selected" : "") . ">Comentario</option>
								</select>
							ordenar: <select id='sColOrdenar'>
										<option value='' >Default</option>
										<option value='asset' " . ( $colOrdenar == "asset" ? "selected" : "") . ">Asset</option>
										<option value='subnumber' " . ( $colOrdenar == "subnumber" ? "selected" : "") . ">Subnumber</option>
										<option value='capitalizedOn' " . ( $colOrdenar == "capitalizedOn" ? "selected" : "") . ">Capitalized On</option>
										<option value='description' " . ( $colOrdenar == "description" ? "selected" : "") . ">Description</option>" .
										/*
										<option value='acquisVal' " . ( $colOrdenar == "acquisVal" ? "selected" : "") . ">Acquis. Val</option>
										<option value='accumDep' " . ( $colOrdenar == "accumDep" ? "selected" : "") . ">Accum. Dep</option>
										*/
										"<option value='bookVal' " . ( $colOrdenar == "bookVal" ? "selected" : "") . ">Book Val</option>
										<option value='currency' " . ( $colOrdenar == "currency" ? "selected" : "") . ">Currency</option>
										<option value='plant' " . ( $colOrdenar == "plant" ? "selected" : "") . ">Plant</option>
										<option value='respCostCenter' " . ( $colOrdenar == "respCostCenter" ? "selected" : "") . ">Resp. Cost Center</option>
										<option value='pm' " . ( $colBuscar == "pm" ? "selected" : "") . ">PM</option>
										<option value='order' " . ( $colOrdenar == "order" ? "selected" : "") . ">Order</option>
										<option value='expiredUseFulLife' " . ( $colOrdenar == "expiredUseFulLife" ? "selected" : "") . ">Expired useful life</option>
										<option value='ubicacion' " . ( $colOrdenar == "ubicacion" ? "selected" : "") . ">Ubicación</option>
										<option value='marca' " . ( $colOrdenar == "marca" ? "selected" : "") . ">Marca</option>
										<option value='modelo' " . ( $colOrdenar == "modelo" ? "selected" : "") . ">Modelo</option>
										<option value='noSerie' " . ( $colOrdenar == "noSerie" ? "selected" : "") . ">noSerie</option>
										<option value='pedimento' " . ( $colOrdenar == "pedimento" ? "selected" : "") . ">Pedimento</option>
										<option value='statusEtiquetado' " . ( $colOrdenar == "statusEtiquetado" ? "selected" : "") . ">Status de etiquetado</option>
										<option value='statusActivo' " . ( $colOrdenar == "statusActivo" ? "selected" : "") . ">Status del Activo</option>
										<option value='origen' " . ( $colOrdenar == "origen" ? "selected" : "") . ">Origen</option>
										<option value='comentario' " . ( $colOrdenar == "comentario" ? "selected" : "") . ">Comentario</option>
									</select>
							direccion: <select id='sOrdeDireccion'>
											<option value='asc' " . ( $ordDireccion == "asc" ? "selected" : "") . ">Ascendente</option>
											<option value='desc' " . ( $ordDireccion == "desc" ? "selected" : "") . ">Descendente</option>
										</select>
							<input type='button' value='Filtrar' onclick='getEventoInventario($idEvento, 0);' />
							<br />
							<form action='informe.php' method='post'>
								<table style='width:620px' border='0'>
									<tr>
										<td>
										Reporte: <select name='tipoReporte'>
													<option value='rep1'>Inventario SAP faltante</option>
													<option value='rep4'>Inventario tablet</option>
													<option value='rep2'>Inventario tablet sin registro SAP</option>
													<option value='rep3'>Todo el evento</option>
												</select>
										</td>
										<td>
											<input name='Excel' value='Genera' src='img/excel-icon.png' type='image' width='35' height='35'>
											<input type='hidden' name='idEvento' value='$idEvento' >
										</td>
									</tr>
								</table>
							</form>
						</p>
						<br/>
					</div>
				</div>   
				%listadoPaginas%                 
				<table class='tablaDatos' border='1' cellpadding='0' cellspacing='0'>
                    	<tr class='rowTitulo'>
                        	<td><a onclick='addInventario($idEvento);'><img src='img/add.png' /></a></td>
							<td>Tipo</td>
                            <td>Fotografía</td>
                            <td>Asset</td>
                            <td>Subnumber</td>
                            <td>Capitalized<br />On</td>
                            <td>Description</td>" .
							/*
                            <td>Acquis. Val</td>
                            <td>Accum. Dep</td>
							*/
                            "<td>Book Val</td>
                            <td>Currency</td>
                            <td>Plant</td>
                            <td>Resp.<br />cost.<br />Center</td>
							<td>PM</td>
                            <td>Order</td>
                            <td>Expired<br />useful<br />life</td>
                            <td>Ubicación</td>
                            <td>Marca</td>
                            <td>Modelo</td>
                            <td>No.<br />Serie</td>
                            <td>Pedimento</td>
                            <td>Status <br />de<br />etiquetado</td>
                            <td>Status <br />del<br />activo</td>
                            <td>Origen</td>
                            <td>Comentarios</td>
                        </tr>";
			$db = getConexion($cnx);
				if($db->conecta()){
					//$pagina--;
					$colOrdenar = ( $colOrdenar == 'order' ? '`order`' : $colOrdenar );
					$query = "select *, date_format( capitalizedOn, '%d/%m/%Y' ) capitalizedOn2 from eventosInventario where eventos_id = $idEvento and subnumber = '0'" . 
							( $buscar != "" ? " and $colBuscar like '%$buscar%'" : '' ) .
							( $colOrdenar != "" ? " order by $colOrdenar $ordDireccion" : "" ) .
							" limit " . ( $pagina * REGISTROS_X_PAGINA ) . ", " . REGISTROS_X_PAGINA;
					$link = $db->db_query($query) or error("Error en la consulta: $query");
						while($datos = $db->db_fetch_object($link)){
							$imagen = $datos->asset . '.jpg';
							$path = "imgInventario/";
								if($datos->origenRegistro != "Inventario")
									$path = "imgInventario/";
								if(!file_exists($path . $imagen)){
									$path =  "imgInventario/";
									$imagen = "no-image.png";
								}
							$t .= "<tr>
									<td><a onclick='borrar({$datos->eventos_id},{$datos->eventosInventario_id})'><img src='img/delete.png' heigth='" . ICONOS_ALTO . "' width='" . ICONOS_ANCHO . "' /></a>
									&nbsp;<a onclick='editar({$datos->eventos_id},{$datos->eventosInventario_id})'><img src='img/editar.png' heigth='" . ICONOS_ALTO . "' width='" . ICONOS_ANCHO . "'/></a></td>
									<td>{$datos->origenRegistro}</td>
									<td><a onclick=\"verFoto('$path$imagen')\">
										<img src='$path$imagen' height='" . IMAGENES_ALTO . "' wwidth='" . IMAGENES_ANCHO . "'/></a></td>
									<td>{$datos->asset}</td>
									<td>{$datos->subnumber}</td>
									<td>{$datos->capitalizedOn2}</td>
									<td>{$datos->description}</td>" .
									/*
									<td>" . number_format($datos->acquisVal, 2) . "</td>
									<td>" . number_format($datos->accumDep, 2) . "</td>
									*/
									"<td>" . number_format($datos->bookVal, 2) . "</td>
									<td>{$datos->currency}</td>
									<td>{$datos->plant}</td>
									<td>{$datos->respCostCenter}</td>
									<td>{$datos->pm}</td>
									<td>{$datos->order}</td>
									<td>{$datos->expiredUsefulLife}</td>
									<td>{$datos->ubicacion}</td>
									<td>{$datos->marca}</td>
									<td>{$datos->modelo}</td>
									<td>{$datos->noSerie}</td>
									<td>{$datos->pedimento}</td>
									<td>{$datos->statusEtiquetado}</td>
									<td>{$datos->statusActivo}</td>
									<td>{$datos->origen}</td>
									<td>{$datos->comentarios}</td>
								</tr>";
						}
					$query = "select 
								(select count(*) from eventosInventario where eventos_id = $idEvento and origenRegistro = 'Tablet' and subnumber = '0') registrosTablet,
								(select count(*) from eventosInventario where eventos_id = $idEvento and origenRegistro = 'Inventario' and subnumber = '0' ) registrosInventario";
					$link = $db->db_query( $query ) or error("Error en la consulta: $query");
					$datos = $db->db_fetch_object($link);
						if($datos->registrosInventario > 0)
							$avance = "Avance: [{$datos->registrosTablet} de {$datos->registrosInventario} - " . number_format((($datos->registrosTablet / $datos->registrosInventario)*100), 4) . "%]";
						else
							$avance = "Avance: [{$datos->registrosTablet} de {$datos->registrosInventario} - 0.0000%]";
					$query = "select count(id) noRegistros from eventosInventario where eventos_id = $idEvento and subnumber = '0'" . 
							( $buscar != "" ? " and $colBuscar like '%$buscar%'" : '' ) .
							( $colOrdenar != "" ? " order by $colOrdenar $ordDireccion" : "" );
					$link = $db->db_query( $query ) or error("Error en la consulta: $query");
					$datos = $db->db_fetch_object($link);
					$total = ceil ( $datos->noRegistros / REGISTROS_X_PAGINA );
					$listaPaginas = "";
						for($x = 0; $x < $total && $total > 1; $x++){
								if($pagina == $x){
									//$listaPaginas .= "<span class='pag_actual'>$x</span>";
									$listaPaginas .= "<option value='$x' selected='selected'>$x</option>";
								}else{
									//$listaPaginas .= "<a href='Resultados' class='pag_selecionar' onclick=\"getInventario($x)\">$x</a>";
									$listaPaginas .= "<option value='$x'>$x</option>";
								}
						}
					$listaPaginas = ( $listaPaginas != "" ? "Página:<select id='sPagina' class='filtro' onchange=\"getEventoInventario($idEvento, this.value)\">" . $listaPaginas . "</select>&nbsp;" : "" );
					$listaPaginas = "<div width='100%' align='center'>" . $listaPaginas . "Registros:" . number_format($datos->noRegistros, 0) . " $avance</div>";
					$t = str_replace("%listadoPaginas%", $listaPaginas, $t);
				}
			$t .= "</table>";
			return $t;
	}

	if( isset( $_FILES['imagen'] ) && isset( $_POST['asset'] ) ){
		if( $_FILES['imagen']['error'] == 0 ){
			$file_opc = pathinfo ( $_FILES['imagen']['name'] );
				if ( in_array ( strtolower ( $file_opc['extension'] ), array ( 'jpg', 'jpeg' ) ) ){
					global $cnx;
					$db = getConexion( $cnx );
						if( $db->conecta() ){
							//$nombreImagen = $_SERVER["HTTP_HOST"] . DS . $file_opc . ;
							$asset = $_POST['asset'];
							$nombreImagen = $asset . ".jpg";// . $file_opc['extension'];
							$folder = "." . DIRECTORY_SEPARATOR . "imgInventario" . DIRECTORY_SEPARATOR;
							move_uploaded_file($_FILES['imagen']['tmp_name'], $folder . $nombreImagen );
							$fotoPath = "http://" . $_SERVER["HTTP_HOST"] . DS . "Inventarios" . DS . "imgInventario" . DS . $nombreImagen;
							$query = "update inventario set foto = '$fotoPath' where asset = '$asset'";
							$db->db_query( $query ) or die ( 'Error en la consulta: $query, ' );
							//$fotoEmpleado = str_replace( DS, "/", $fotoEmpleado );
							die( "Foto Actualizada." );
						}
				}
		}
		die ( 'Error' );
	}else
	if ( isset ( $_POST['cmd'] ) or isset( $_GET['cmd'] ) ){
		$cmd = parametro('cmd');
		switch ( $cmd ){
			case 'getInventario':{
				$buscar = parametro( 'buscar' );
				$buscarCol = parametro( 'buscarCol' );
				$ordenCol = parametro ( 'ordenCol' );
				$ordenDir = parametro ( 'ordenDir' );
				$pagina = parametro ( 'pagina' );
				//echo "getInventario( $pagina, $buscar, $buscarCol, $ordenCol, $ordenDir);";
				echo getInventario( $pagina, $buscar, $buscarCol, $ordenCol, $ordenDir);
				break;
			}
			case 'addInventario':{
				global $cnx;
				$error = "";
				if(isset($_POST['asset'])){
					// Agregar el inventario y terminar o mandar error
					$asset = $_POST['asset'];
					$subnumber = $_POST['subnumber'];
					$capitalizedOnY = $_POST['capitalizedOnY'];
					$capitalizedOnM = $_POST['capitalizedOnM'];
					$capitalizedOnD = $_POST['capitalizedOnD'];
					$description = $_POST['description'];
					$acquisVal = $_POST['acquisVal'];
					$accumDep = $_POST['accumDep'];
					$bookVal = $_POST['bookVal'];
					$currency = $_POST['currency'];
					$plant = $_POST['plant'];
					$respCostCenter = $_POST['respCostCenter'];
					$pm = $_POST['pm'];
					$order = $_POST['order'];
					$expiredUsefulLife = $_POST['expiredUsefulLife'];
					$ubicacion = $_POST['ubicacion'];
					$marca = $_POST['marca'];
					$modelo = $_POST['modelo'];
					$noSerie = $_POST['noSerie'];
					$pedimento = $_POST['pedimento'];
					$statusEtiquetado = $_POST['statusEtiquetado'];
					$statusActivo = $_POST['statusActivo'];
					$origen = $_POST['origen'];
					$comentarios = $_POST['comentarios'];
					$query = "insert into inventario(asset, subnumber, capitalizedOn, description, acquisVal, accumDep, bookVal, currency, plant, respCostCenter, `order`, expiredUsefulLife, 
													ubicacion, marca, modelo, noSerie, pedimento, statusEtiquetado, statusActivo, origen, comentarios, pm)values
								('$asset', '$subnumber', '$capitalizedOnY-$capitalizedOnM-$capitalizedOnD', '$description', '$acquisVal', '$accumDep', '$bookVal', '$currency', '$plant',
								'$respCostCenter', '$order', '$expiredUsefulLife', '$ubicacion', '$marca', '$modelo', '$noSerie', '$pedimento', '$statusEtiquetado', '$statusActivo',
								'$origen', '$comentarios', '$pm')";
					$db = getConexion($cnx);
						if($db->conecta()){
							if($db->db_query($query))
								die ( '<div align="center" id="errordial" class="errordial" >Maquina agregada correctamente</div><script type="text/javascript">actualizar=true;</script>' );
							else
								$error = "Error en la consulta: $query. Error Mysql: " . mysql_error();
						}else{
							$error = "Problemas de conexion.";
						}
				}
				$anio_hoy = date("Y");
				$fecha = "Año: <select name='capitalizedOnYear'>";
					for( $anio = $anio_hoy - 50; $anio < $anio_hoy; $anio++)
						 $fecha .= "<option value='" . $anio . "'>$anio</option>";
				$fecha .= "</select>";
				$fecha .= " Mes: <select name='capitalizedOnMonth'>";
					for( $mes = 1; $mes <= 12; $mes++)
						$fecha .= "<option value='" . ( $mes < 10 ? "0$mes" : $mes ) . "' " . ( $mes == 1 ? "selected" : "" ) . ">$mes</option>";
				$fecha .= "</select>";
				$fecha .= " Dia: <select name='capitalizedOnDay'>";
					for( $dia = 1; $dia <= 31; $dia++)
						$fecha .= "<option value='" . ( $dia < 10 ? "0$dia" : $dia ) . "' " . ( $dia == 1 ? "selected" : "" ) . ">$dia</option>";
				$fecha .= "</select>";
				$fecha = "<input id='capitalizedOn' type='text' />";
				$resultado = '<div align="center" id="errordial" class="errordial" style="display:' . ($error != '' ? 'block' : 'none' ) . '">' . $error . '</div>
							<br /><br />
							<form action="javascript:agregaInventario(document.registrarInventario);" method="post" name="registrarInventario" onsubmit="return validarInventario(this)">
							<table width="500" border="0" class="addInventario">
								<tr><td>Asset:</td><td width="305"><input name="asset" type="text" size="10" maxlength="20" value="" /></td></tr>
								<tr><td>Subnumber:</td><td width="305"><input name="subnumber" type="text" size="10" maxlength="10" value="" /></td></tr>
								<tr><td>Capitalized On:</td><td width="305">' . $fecha . '</td></tr>
								<tr><td>Description:</td><td width="305"><textarea name="description" cols="30" rows="5"></textarea></td></tr>
								<tr><td>Acquis Val.:</td><td width="305"><input name="acquisVal" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Accum Dep.:</td><td width="305"><input name="accumDep" type="text" size="15" maxlength="100" value="" /></td></tr>
								<tr><td>Book Val.:</td><td width="305"><input name="bookVal" type="text" size="15" maxlength="100" value="" /></td></tr>
								<tr><td>Currency:</td><td width="305"><input name="currency" type="text" size="15" maxlength="100" value="" /></td></tr>
								<tr><td>Plant:</td><td width="305"><input name="plant" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Resp. Cost Center:</td><td width="305"><input name="respCostCenter" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>PM:</td><td width="305"><input name="pm" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Order:</td><td width="305"><input name="order" type="text" size="20" maxlength="100" value="" /></td></tr>
								<tr><td>Expired Useful Life:</td><td width="305"><input name="expiredUsefulLife" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Ubicación:</td><td width="305"><input name="ubicacion" type="text" size="30" maxlength="100" value="" /></td></tr>
								<tr><td>Marca:</td><td width="305"><input name="marca" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Modelo:</td><td width="305"><input name="modelo" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>No Serie:</td><td width="305"><input name="noSerie" type="text" size="25" maxlength="100" value="" /></td></tr>
								<tr><td>Pedimento:</td><td width="305"><input name="pedimento" type="text" size="20" maxlength="100" value="" /></td></tr>
								<tr><td>Status de Etiquetado:</td><td width="305"><input name="statusEtiquetado" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Status del Activo:</td><td width="305"><select name="statusActivo">
																					<option value="Activo">Activo</option>
																					<option value="Idle operation">Idle Operation</option>
																					<option value="Idle equipment">Idle Equipment</option>
																					<option value="Idle in place">Idle in Place</option>
																					<option value="Scrap">Scrap</option>
																				</select>
																</td></tr>
								<tr><td>Origen:</td><td width="305"><select name="origen">
																					<option value="SAP">SAP</option>
																					<option value="Inv. Interno">Inventario Interno</option>
																				</select>
																</td></tr>
								<tr><td>Comentarios:</td><td width="305"><textarea name="comentarios" cols="30" rows="5"></textarea></td></tr>
								<tr>
									<td colspan="60" align="right"><input name="Guardar" value="Guardar" src="img/guardar.png" type="image"><input type="hidden" name="idcliente"></td>
								</tr>
							</table>
						</form>';
				echo $resultado;
				break;
			}
			case 'editInventario':{
				global $cnx;
				$error = "";
				if(isset($_POST['id'])){
					$id = $_POST['id'];
					$db = getConexion($cnx);
						if($db->conecta()){
							$query = "select *, extract(year from capitalizedOn) anio, extract(month from capitalizedOn) mes, extract(day from capitalizedOn) dia from inventario where id = $id";
							$link = $db->db_query($query) or error("Error en la consulta: $query");
							$datos = $db->db_fetch_object($link);
								if($datos->id > 0){
									$asset = $datos->asset;
									$subnumber = $datos->subnumber;
									$capitalizedOnY = $datos->anio;
									$capitalizedOnM = $datos->mes;
									$capitalizedOnD = $datos->dia;
									$description = $datos->description;
									$acquisVal = $datos->acquisVal;
									$accumDep = $datos->accumDep;
									$bookVal = $datos->bookVal;
									$currency = $datos->currency;
									$plant = $datos->plant;
									$respCostCenter = $datos->respCostCenter;
									$pm = $datos->pm;
									$order = $datos->order;
									$expiredUsefulLife = $datos->expiredUsefulLife;
									$ubicacion = $datos->ubicacion;
									$marca = $datos->marca;
									$modelo = $datos->modelo;
									$noSerie = $datos->noSerie;
									$pedimento = $datos->pedimento;
									$statusEtiquetado = $datos->statusEtiquetado;
									$statusActivo = $datos->statusActivo;
									$origen = $datos->origen;
									$comentarios = $datos->comentarios;
								}
						}
				}else
					if(isset($_POST['idMaquina'])){
						// Agregar el inventario y terminar o mandar error
						$asset = $_POST['asset'];
						$subnumber = $_POST['subnumber'];
						$capitalizedOnY = $_POST['capitalizedOnY'];
						$capitalizedOnM = $_POST['capitalizedOnM'];
						$capitalizedOnD = $_POST['capitalizedOnD'];
						$description = $_POST['description'];
						$acquisVal = $_POST['acquisVal'];
						$accumDep = $_POST['accumDep'];
						$bookVal = $_POST['bookVal'];
						$currency = $_POST['currency'];
						$plant = $_POST['plant'];
						$respCostCenter = $_POST['respCostCenter'];
						$pm = $_POST['pm'];
						$order = $_POST['order'];
						$expiredUsefulLife = $_POST['expiredUsefulLife'];
						$ubicacion = $_POST['ubicacion'];
						$marca = $_POST['marca'];
						$modelo = $_POST['modelo'];
						$noSerie = $_POST['noSerie'];
						$pedimento = $_POST['pedimento'];
						$statusEtiquetado = $_POST['statusEtiquetado'];
						$statusActivo = $_POST['statusActivo'];
						$origen = $_POST['origen'];
						$comentarios = $_POST['comentarios'];
						$query = "update inventario 
										set asset = '$asset', subnumber = '$subnumber', capitalizedOn = '$capitalizedOnY-$capitalizedOnM-$capitalizedOnD', description = '$description',
										acquisVal = '$acquisVal', accumDep = '$accumDep', bookVal = '$bookVal', currency = '$currency', plant = '$plant', respCostCenter = '$respCostCenter',
										`order` = '$order', expiredUsefulLife = '$expiredUsefulLife', ubicacion = '$ubicacion', marca = '$marca', modelo = '$modelo', noSerie = '$noSerie',
										pedimento = '$pedimento', statusEtiquetado = '$statusEtiquetado', statusActivo = '$statusActivo', origen = '$origen', comentarios = '$comentarios',
										pm = '$pm'
									where id = " . $_POST['idMaquina'];
						$db = getConexion($cnx);
							if($db->conecta()){
								if($db->db_query($query))
									die ( '<div align="center" id="errordial" class="errordial" >Maquina guardada correctamente</div><script type="text/javascript">actualizar=true;</script>' );
								else
									$error = "Error en la consulta: $query. Error Mysql: " . mysql_error();
							}else{
								$error = "Problemas de conexion.";
							}
					}else die ( "No hay accion." );
				$anio_hoy = date("Y");
				$fecha = "Año: <select name='capitalizedOnYear'>";
					for( $anio = $anio_hoy - 50; $anio < $anio_hoy; $anio++)
						 $fecha .= "<option value='" . $anio . "' " . ($capitalizedOnY == $anio ? "selected" : "") . ">$anio</option>";
				$fecha .= "</select>";
				$fecha .= " Mes: <select name='capitalizedOnMonth'>";
					for( $mes = 1; $mes <= 12; $mes++)
						$fecha .= "<option value='" . ( $mes < 10 ? "0$mes" : $mes ) . "' " . ($capitalizedOnM == $mes ? "selected" : "") . ">$mes</option>";
				$fecha .= "</select>";
				$fecha .= " Dia: <select name='capitalizedOnDay'>";
					for( $dia = 1; $dia <= 31; $dia++)
						$fecha .= "<option value='" . ( $dia < 10 ? "0$dia" : $dia ) . "' " . ($capitalizedOnD == $dia ? "selected" : "") . ">$dia</option>";
				$fecha .= "</select>";
				$fecha = "<input id='capitalizedOn' type='text' value='$capitalizedOnY-" . 
																			( $capitalizedOnM < 10 ? "0$capitalizedOnM-" : "$capitalizedOnM-" ) . 
																			( $capitalizedOnD < 10 ? "0$capitalizedOnD" : "$capitalizedOnD" ) . "' />";
				$resultado = '<div align="center" id="errordial" class="errordial" style="display:' . ($error != '' ? 'block' : 'none' ) . '">' . $error . '</div>
							<br /><br />
							<form action="javascript:editaInventario(document.editarInventario);" method="post" name="editarInventario" onsubmit="return validarInventario(this)">
							<table width="500" border="0" class="addInventario">
								<tr><td>Asset:</td><td width="305"><input name="asset" type="text" size="10" maxlength="20" value="' . $asset . '" /></td></tr>
								<tr><td>Subnumber:</td><td width="305"><input name="subnumber" type="text" size="10" maxlength="10" value="' . $subnumber . '" /></td></tr>
								<tr><td>Capitalized On:</td><td width="305">' . $fecha . '</td></tr>
								<tr><td>Description:</td><td width="305"><textarea name="description" cols="30" rows="5">' . $description . '</textarea></td></tr>
								<tr><td>Acquis Val.:</td><td width="305"><input name="acquisVal" type="text" size="10" maxlength="100" value="' . $acquisVal . '" /></td></tr>
								<tr><td>Accum Dep.:</td><td width="305"><input name="accumDep" type="text" size="15" maxlength="100" value="' . $accumDep . '" /></td></tr>
								<tr><td>Book Val.:</td><td width="305"><input name="bookVal" type="text" size="15" maxlength="100" value="' . $bookVal . '" /></td></tr>
								<tr><td>Currency:</td><td width="305"><input name="currency" type="text" size="15" maxlength="100" value="' . $currency . '" /></td></tr>
								<tr><td>Plant:</td><td width="305"><input name="plant" type="text" size="10" maxlength="100" value="' . $plant . '" /></td></tr>
								<tr><td>Resp. Cost Center:</td><td width="305"><input name="respCostCenter" type="text" size="10" maxlength="100" value="' . $respCostCenter . '" /></td></tr>
								<tr><td>PM:</td><td width="305"><input name="pm" type="text" size="10" maxlength="100" value="' . $pm . '" /></td></tr>
								<tr><td>Order:</td><td width="305"><input name="order" type="text" size="20" maxlength="100" value="' . $order . '" /></td></tr>
								<tr><td>Expired Useful Life:</td><td width="305"><input name="expiredUsefulLife" type="text" size="10" maxlength="100" value="' . $expiredUsefulLife . '" /></td></tr>
								<tr><td>Ubicación:</td><td width="305"><input name="ubicacion" type="text" size="30" maxlength="100" value="' . $ubicacion . '" /></td></tr>
								<tr><td>Marca:</td><td width="305"><input name="marca" type="text" size="10" maxlength="100" value="' . $marca . '" /></td></tr>
								<tr><td>Modelo:</td><td width="305"><input name="modelo" type="text" size="10" maxlength="100" value="' . $modelo . '" /></td></tr>
								<tr><td>No Serie:</td><td width="305"><input name="noSerie" type="text" size="25" maxlength="100" value="' . $noSerie . '" /></td></tr>
								<tr><td>Pedimento:</td><td width="305"><input name="pedimento" type="text" size="20" maxlength="100" value="' . $pedimento . '" /></td></tr>
								<tr><td>Status de Etiquetado:</td><td width="305"><input name="statusEtiquetado" type="text" size="10" maxlength="100" value="' . $statusEtiquetado . '" /></td></tr>
								<tr><td>Status del Activo:</td><td width="305"><select name="statusActivo">
																					<option value="Activo" ' . ($statusActivo == "Activo" ? "selected" : "" ) . '>Activo</option>
																					<option value="Idle operation" ' . ($statusActivo == "Idle operation" ? "selected" : "" ) . '>Idle Operation</option>
																					<option value="Idle equipment" ' . ($statusActivo == "Idle equipment" ? "selected" : "" ) . '>Idle Equipment</option>
																					<option value="Idle in place" ' . ($statusActivo == "Idle in place" ? "selected" : "" ) . '>Idle in Place</option>
																					<option value="Scrap" ' . ($statusActivo == "Scrap" ? "selected" : "" ) . '>Scrap</option>
																				</select>
																</td></tr>
								<tr><td>Origen:</td><td width="305"><select name="origen">
																					<option value="SAP" ' . ($origen == "SAP" ? "selected" : "" ) . '>SAP</option>
																					<option value="Inv. Interno" ' . ($origen == "Inv. Interno" ? "selected" : "" ) . '>Inventario Interno</option>
																				</select>
																</td></tr>
								<tr><td>Comentarios:</td><td width="305"><textarea name="comentarios" cols="30" rows="5">' . $comentarios . '</textarea></td></tr>
								<tr>
									<td colspan="60" align="right"><input name="Guardar" value="Guardar" src="img/guardar.png" type="image"><input type="hidden" name="idMaquina" value="' . $id . '"></td>
								</tr>
							</table>
						</form>';
				echo $resultado;
				break;
			}
			case 'delInventario':{
				global $cnx;
					if(isset($_POST['idMaquina'])){
						$id = $_POST['idMaquina'];
						$db = getConexion($cnx);
							if($db->conecta()){
								$query = "delete from inventario where id = $id";
									if($db->db_query($query)){
										die( '<div align="center" id="errordial" class="errordial" >Maquina eliminada correctamente.</div>' );
									}else{
										die( '<div align="center" id="errordial" class="errordial" >Error en la consulta ' . $query . '</div>' );
									}
							}else{
								die( '<div align="center" id="errordial" class="errordial" >Error de Conexion.</div>' );
							}
					}else{
						die( '<div align="center" id="errordial" class="errordial" >La maquina no existe.</div>' );
					}
				break;
			}
			case 'editarFoto':{
					if( isset( $_POST['asset'] ) ){
						$asset = $_POST['asset'];
						echo "<form action='javascript:enviarFoto(document.frmEditarFoto);' name='frmEditarFoto' enctype='multipart/form-data' method='post'>
									<div align='center'><img src='./imgInventario/$asset.jpg' /></div><br>
									Nueva Imagen: 
									<input type='file' id='imagen' name='imagen' /><br>
									<input name='asset' id='asset' value='$asset' type='hidden' />
									<input name='submit' id='submit' value='Guardar' src='img/guardar.png' type='image' />
								</form>
								" ;
					}
				break;
			}
			// ******************************************** USUARIOS ***************************************
			case 'getUsuarios':{
				$buscar = parametro( 'buscar' );
				$buscarCol = parametro( 'buscarCol' );
				$ordenCol = parametro ( 'ordenCol' );
				$ordenDir = parametro ( 'ordenDir' );
				$pagina = parametro ( 'pagina' );
				echo getUsuarios( $pagina, $buscar, $buscarCol, $ordenCol, $ordenDir);
				break;
			}
			case 'addUsuario':{
				global $cnx;
				$error = "";
				if(isset($_POST['login'])){
					// Agregar el inventario y terminar o mandar error
					$login = $_POST['login'];
					$pass = $_POST['pass'];
					$nombre = mysql_real_escape_string($_POST['nombre']);
					$email = mysql_real_escape_string($_POST['email']);
					$usrSoloTablet = $_POST['soloTablet'];
					$db = getConexion($cnx);
						if($db->conecta()){
							$query = "select count(login) noLogins from usuarios where login = '$login'";
							$link = $db->db_query($query) or error("Error en la consulta: $query");
							$datos = $db->db_fetch_object($link);
								if($datos->noLogins > 0){
									die ( '<div align="center" id="errordial" class="errordial">El login: ' . $login . ' ya existe.</div>' );
								}else{
									$query = "insert into usuarios(login, pass, nombre, email, usrTablet)values
												('$login', '$pass', '$nombre', '$email', $usrSoloTablet)";
										if($db->db_query($query))
											die ( '<div align="center" id="errordial" class="errordial" >Usuario agregado correctamente</div>' );
										else
											$error = "Error en la consulta: $query. Error Mysql: " . mysql_error();
								}
						}else{
							$error = "Problemas de conexion.";
						}
				}
				$resultado = '<div align="center" id="errordial" class="errordial" style="display:' . ($error != '' ? 'block' : 'none' ) . '">' . $error . '</div>
							<br /><br />
							<form action="javascript:agregaUsuario(document.registrarUsuario);" method="post" name="registrarUsuario" onsubmit="return validarUsuario(this)">
							<table width="500" border="0" class="addInventario">
								<tr><td>Login:</td><td width="305"><input name="login" type="text" size="10" maxlength="20" value="" /></td></tr>
								<tr><td>Nombre:</td><td width="305"><input name="nombre" type="text" size="40" maxlength="50" value="" /></td></tr>
								<tr><td>Password:</td><td width="305"><input name="pass" type="password" size="10" maxlength="50" value="" /></td></tr>
								<tr><td>Re-Password:</td><td width="305"><input name="rePass" type="password" size="10" maxlength="50" value="" /></td></tr>
								<tr><td>Email:</td><td width="305"><input name="email" type="email" size="20" maxlength="50" value="" /></td></tr>
								<tr><td>Solo usara Tablet:</td><td width="305"><input name="soloTablet" type="checkbox" /></td></tr>
								<tr>
									<td colspan="60" align="right"><input name="Guardar" value="Guardar" src="img/guardar.png" type="image"><input type="hidden" name="idcliente"></td>
								</tr>
							</table>
						</form>';
				echo $resultado;
				break;
			}
			case 'editUsuario':{
				global $cnx;
				$error = "";
				if(isset($_POST['loginUsuario'])){
					$login = $_POST['loginUsuario'];
					$db = getConexion($cnx);
						if($db->conecta()){
							$query = "select * from usuarios where login = '$login'";
							$link = $db->db_query($query) or error("Error en la consulta: $query");
							$datos = $db->db_fetch_object($link);
								if($datos->login == $login){
									$login = $datos->login;
									$nombre = $datos->nombre;
									$email = $datos->email;
									$usrSoloTablet = $datos->usrTablet;
								}
						}
				}else
					if(isset($_POST['login'])){
						// Agregar el inventario y terminar o mandar error
						$login = $_POST['login'];
						$nombre = mysql_real_escape_string($_POST['nombre']);
						$pass = $_POST['pass'];
						$email = mysql_real_escape_string($_POST['email']);
						$usrSoloTablet = $_POST['soloTablet'];
						$query = "update usuarios 
										set nombre = '$nombre', email = '$email', usrTablet = $usrSoloTablet" . ( $pass != "" ? ", pass = '$pass'" : "" ) .
										" where login = '" . $_POST['login'] . "'";
						$db = getConexion($cnx);
							if($db->conecta()){
								if($db->db_query($query))
									die ( '<div align="center" id="errordial" class="errordial">Usuario guardado correctamente</div>' );
								else
									$error = "Error en la consulta: $query. Error Mysql: " . mysql_error();
							}else{
								$error = "Problemas de conexion.";
							}
					}else die ( "No hay accion." );
				$resultado = '<div align="center" id="errordial" class="errordial" style="display:' . ($error != '' ? 'block' : 'none' ) . '">' . $error . '</div>
							<br /><br />
							<form action="javascript:editaUsuario(document.editarUsuario);" method="post" name="editarUsuario" onsubmit="return validarUsuario(this)">
							<table width="500" border="0" class="addInventario">
								<tr><td>Login:</td><td width="305"><input name="login" type="text" size="10" maxlength="20" value="' . $login . '" /></td></tr>
								<tr><td>Nombre:</td><td width="305"><input name="nombre" type="text" size="40" maxlength="50" value="' . $nombre . '" /></td></tr>
								<tr><td>Password:</td><td width="305"><input name="pass" type="password" size="10" maxlength="50" value="" /></td></tr>
								<tr><td>Re-Password:</td><td width="305"><input name="rePass" type="password" size="10" maxlength="50" value="" /></td></tr>
								<tr><td>Email:</td><td width="305"><input name="email" type="email" size="20" maxlength="50" value="' . $email . '" /></td></tr>
								<tr><td>Solo usara Tablet:</td><td width="305"><input name="soloTablet" type="checkbox" ' . ($usrSoloTablet == '1' ? "checked" : "") . '/></td></tr>
								<tr>
									<td colspan="60" align="right"><input name="Guardar" value="Guardar" src="img/guardar.png" type="image"></td>
								</tr>
							</table>
						</form>';
				echo $resultado;
				break;
			}
			case 'delUsuario':{
				global $cnx;
					if(isset($_POST['loginUsuario'])){
						$login = $_POST['loginUsuario'];
						$db = getConexion($cnx);
							if($db->conecta()){
								$query = "delete from usuarios where login = '$login'";
									if($db->db_query($query)){
										die( '<div align="center" id="errordial" class="errordial" >Usuario eliminado correctamente.</div>' );
									}else{
										die( '<div align="center" id="errordial" class="errordial" >Error en la consulta ' . $query . '</div>' );
									}
							}else{
								die( '<div align="center" id="errordial" class="errordial" >Error de Conexion.</div>' );
							}
					}else{
						die( '<div align="center" id="errordial" class="errordial" >El usuarios no existe.</div>' );
					}
				break;
			}
			// ******************************************** EVENTOS ***************************************
			case 'getEventos':{
				$buscar = parametro( 'buscar' );
				$buscarCol = parametro( 'buscarCol' );
				$ordenCol = parametro ( 'ordenCol' );
				$ordenDir = parametro ( 'ordenDir' );
				$pagina = parametro ( 'pagina' );
				echo getEventos( $pagina, $buscar, $buscarCol, $ordenCol, $ordenDir);
				break;
			}
			case 'addEvento':{
				global $cnx;
				$error = "";
				$fechaInicialA = '';
				$fechaInicialM = '';
				$fechaInicialD = '';
				$fechaFinalA = '';
				$fechaFinalM = '';
				$fechaFinalD = '';


				if(isset($_POST['nombre'])){
//				print_r ($_POST);
//				die();
					// Agregar el inventario y terminar o mandar error
					//$nombre = mysql_real_escape_string($_POST['nombre']);
                    $nombre = $_POST['nombre'];
					$fechaInicialA = $_POST['fechaInicialA'];
					$fechaInicialM = $_POST['fechaInicialM'];
					$fechaInicialD = $_POST['fechaInicialD'];
					$fechaFinalA = $_POST['fechaFinalA'];
					$fechaFinalM = $_POST['fechaFinalM'];
					$fechaFinalD = $_POST['fechaFinalD'];
					$participantes = $_POST['participantes'];
					$db = getConexion($cnx);
						if($db->conecta()){
							$query = "insert into eventos(nombre, fechaInicial, fechaFinal)
										values('$nombre', '$fechaInicialA-$fechaInicialM-$fechaInicialD', '$fechaFinalA-$fechaFinalM-$fechaFinalD')";
								if($db->db_query($query)){
									$idEvento = mysql_insert_id();
									$logins = explode( "|", $participantes );
										for($x = 0; $x < count($logins); $x++){
											$query = "insert into eventosParticipantes(eventos_id, usuarios_login)values($idEvento, '" . $logins[$x] . "')";
												if(!$db->db_query($query))
													die('<div align="center" id="errordial" class="errordial" >Error al agregar el evento.</div>');
										}
									$query = "select * from inventario where statusActivo <> 'Scrap'";
									$link = $db->db_query($query) or error("Error en la consulta: $query");
										while($datos = $db->db_fetch_object($link)){
											$query = "insert into eventosInventario(eventos_id, id, usuarios_login, origenRegistro, asset, subnumber, capitalizedOn, description, acquisVal,
																					accumDep, bookVal, currency, plant, respCostCenter, `order`, expiredUsefulLife, ubicacion, marca,
																					modelo, noSerie, pedimento, foto, statusEtiquetado, statusActivo, origen, comentarios, pm)
														values($idEvento, {$datos->id}, '" . $_SESSION['usrLogeado'] . "', 'Inventario', '{$datos->asset}', '{$datos->subnumber}',
														'{$datos->capitalizedOn}', '" . mysql_real_escape_string($datos->description) . "', '{$datos->acquisVal}', '{$datos->accumDep}', '{$datos->bookVal}',
														'{$datos->currency}', '{$datos->plant}', '{$datos->respCostCenter}', '{$datos->order}', '{$datos->expiredUsefulLife}',
														'{$datos->ubicacion}', '{$datos->marca}', '{$datos->modelo}', '{$datos->noSerie}', '{$datos->pedimento}', '{$datos->foto}',
														'{$datos->statusEtiquetado}', '{$datos->statusActivo}', '{$datos->origen}', '{$datos->comentarios}', '{$datos->pm}')";
												if(!$db->db_query($query))
//													die ( '<div align="center" id="errordial" class="errordial" >Error al guardar el inventario del evento: ' . $query . '</div>' );
													die ( '<div align="center" id="errordial" class="errordial" >Error al guardar el inventario del evento: ' . mysql_error() . '<br><br>' . $query . '</div>' );
										}
									die ( '<div align="center" id="errordial" class="errordial" >Evento agregado correctamente</div>' );
								}else
									$error = "Error en la consulta: $query. Error Mysql: " . mysql_error();
						}else{
							$error = "Problemas de conexion.";
						}
				}
				$anio_hoy = date("Y");
				$fechaI = "Año: <select name='fechaInicialYear'>";
				$fechaF = "Año: <select name='fechaFinalYear'>";
					for( $anio = $anio_hoy; $anio >= $anio_hoy - 2; $anio--){
						 $fechaI .= "<option value='" . $anio . "' " . ($fechaInicialA == $anio ? "selected" : "") . ">$anio</option>";
						 $fechaF .= "<option value='" . $anio . "' " . ($fechaFinalA == $anio ? "selected" : "") . ">$anio</option>";
					}
				$fechaI .= "</select>";
				$fechaF .= "</select>";
				$fechaI .= " Mes: <select name='fechaInicialMonth'>";
				$fechaF .= " Mes: <select name='fechaFinalMonth'>";
					for( $mes = 1; $mes <= 12; $mes++){
						$fechaI .= "<option value='" . ( $mes < 10 ? "0$mes" : $mes ) . "' " . ($fechaInicialM == $mes ? "selected" : "") . ">$mes</option>";
						$fechaF .= "<option value='" . ( $mes < 10 ? "0$mes" : $mes ) . "' " . ($fechaFinalM == $mes ? "selected" : "") . ">$mes</option>";
					}
				$fechaI .= "</select>";
				$fechaF .= "</select>";
				$fechaI .= " Dia: <select name='fechaInicialDay'>";
				$fechaF .= " Dia: <select name='fechaFinalDay'>";
					for( $dia = 1; $dia <= 31; $dia++){
						$fechaI .= "<option value='" . ( $dia < 10 ? "0$dia" : $dia ) . "' " . ($fechaInicialD == $dia ? "selected" : "") . ">$dia</option>";
						$fechaF .= "<option value='" . ( $dia < 10 ? "0$dia" : $dia ) . "' " . ($fechaFinalD == $dia ? "selected" : "") . ">$dia</option>";
					}
				$fechaI .= "</select>";
				$fechaF .= "</select>";
				$fechaI = "<input type='text' id='dpFechaInicial' />";
				$fechaF = "<input type='text' id='dpFechaFinal' />";
				$usuarios = "";
				$db = getConexion($cnx);
					if($db->conecta()){
						$query = "select * from usuarios";
						$link = $db->db_query($query) or error("Error en la consulta: $query");
							while($datos = $db->db_fetch_object($link)){
								$usuarios .= "<tr><td colspan='2'><input type='checkbox' onclick=\"clickParticipante('{$datos->login}');\" /> ({$datos->login}) {$datos->nombre}</td></tr>";
							}
					}
				$resultado = '<div align="center" id="errordial" class="errordial" style="display:' . ($error != '' ? 'block' : 'none' ) . '">' . $error . '</div>
							<br /><br />
							<form action="javascript:agregaEvento(document.registrarEvento);" method="post" name="registrarEvento" onsubmit="return validarEvento(this)">
							<table width="500" border="0" class="addInventario">
								<tr><td>Nombre:</td><td width="305"><input name="nombre" type="text" size="40" maxlength="50" value="" /></td></tr>
								<tr><td>Fecha Inicial:</td><td width="305">' . $fechaI . '</td></tr>
								<tr><td>Fecha Final:</td><td width="305">' . $fechaF . '</td></tr>
								<tr><td><b>Participantes:</b></td><td></td></tr>' .
								$usuarios .
								'<tr>
									<td colspan="60" align="right"><input name="Guardar" value="Guardar" src="img/guardar.png" type="image"><input type="hidden" name="participantes"></td>
								</tr>
							</table>
						</form>';
				echo $resultado;
				break;
			}
			case 'editEvento':{
				global $cnx;
				$error = "";
				if(isset($_POST['idEvento'])){
					$id = $_POST['idEvento'];
					$db = getConexion($cnx);
						if($db->conecta()){
							$query = "select *, extract(year from fechaInicial) fiYear, extract(month from fechaInicial) fiMonth, extract(day from fechaInicial) fiDay,
												extract(year from fechaFinal) ffYear, extract(month from fechaFinal) ffMonth, extract(day from fechaFinal) ffDay 
										from eventos where id = '$id'";
							$link = $db->db_query($query) or error("Error en la consulta: $query");
							$datos = $db->db_fetch_object($link);
								if($datos->id == $id){
									$nombre = $datos->nombre;
									$fiYear = $datos->fiYear;
									$fiMonth = $datos->fiMonth;
									$fiDay = $datos->fiDay;
									$ffYear = $datos->ffYear;
									$ffMonth = $datos->ffMonth;
									$ffDay = $datos->ffDay;
									$participantes = "";
									$query = "select usuarios_login from eventosParticipantes where eventos_id = $id";
									$link = $db->db_query($query) or error("Error en la consulta: $query");
										while($datos = $db->db_fetch_object($link)){
											$participantes .= "[{$datos->usuarios_login}]";
										}
									$query = "select * from usuarios";
									$link = $db->db_query($query) or error("Error en la consulta: $query");
										while($datos = $db->db_fetch_object($link)){
											$usuarios .= "<tr><td colspan='2'><input type='checkbox' onclick=\"clickParticipante('{$datos->login}');\" " .
															(strpos($participantes, "[{$datos->login}]") !== false ? "checked" : ""  ) . 
														"/> ({$datos->login}) {$datos->nombre}</td></tr>";
										}
								}
						}
				}else
					if(isset($_POST['idEdit'])){
						// Agregar el y terminar o mandar error
                        //$nombre = mysql_real_escape_string($_POST['nombre']);
						$nombre = $_POST['nombre'];
						$fechaInicialY = $_POST['fechaInicialY'];
						$fechaInicialM = $_POST['fechaInicialM'];
						$fechaInicialD = $_POST['fechaInicialD'];
						$fechaFinalY = $_POST['fechaFinalY'];
						$fechaFinalM = $_POST['fechaFinalM'];
						$fechaFinalD = $_POST['fechaFinalD'];
						$participantes = $_POST['participantes'];
						$query = "update eventos 
										set nombre = '$nombre', fechaInicial = '$fechaInicialY-$fechaInicialM-$fechaInicialD', 
										fechaFinal = '$fechaFinalY-$fechaFinalM-$fechaFinalD'
									where id = '" . $_POST['idEdit'] . "'";
						$db = getConexion($cnx);
							if($db->conecta()){
								if($db->db_query($query)){
									$query = "delete from eventosParticipantes where eventos_id = " . $_POST['idEdit'];
									$db->db_query($query) or error("Error en la consulta: $query");
									$listParticipantes = explode("|", $participantes);
										foreach( $listParticipantes as $participante){
											$query = "insert into eventosParticipantes(eventos_id, usuarios_login)values(" . $_POST['idEdit'] . ", '$participante')";
												if(!$db->db_query($query)){
													$error = "Error en la consulta: $query. Error Mysql: " . mysql_error();
												}
										}
									die ( '<div align="center" id="errordial" class="errordial">Evento guardado correctamente</div>' );
								}else
									die("Error en la consulta: $query. Error Mysql: " . mysql_error());
							}else{
								die("Problemas de conexion.");
							}
					}else die ( "No hay accion." );
				$anio_hoy = date("Y");
				$fechaI = "Año: <select name='fechaInicialYear'>";
				$fechaF = "Año: <select name='fechaFinalYear'>";
					for( $anio = $anio_hoy; $anio >= $anio_hoy - 2; $anio--){
						 $fechaI .= "<option value='" . $anio . "' " . ($fiYear == $anio ? "selected" : "") . ">$anio</option>";
						 $fechaF .= "<option value='" . $anio . "' " . ($ffYear == $anio ? "selected" : "") . ">$anio</option>";
					}
				$fechaI .= "</select>";
				$fechaF .= "</select>";
				$fechaI .= " Mes: <select name='fechaInicialMonth'>";
				$fechaF .= " Mes: <select name='fechaFinalMonth'>";
					for( $mes = 1; $mes <= 12; $mes++){
						$fechaI .= "<option value='" . ( $mes < 10 ? "0$mes" : $mes ) . "' " . ($fiMonth == $mes ? "selected" : "") . ">$mes</option>";
						$fechaF .= "<option value='" . ( $mes < 10 ? "0$mes" : $mes ) . "' " . ($ffMonth == $mes ? "selected" : "") . ">$mes</option>";
					}
				$fechaI .= "</select>";
				$fechaF .= "</select>";
				$fechaI .= " Dia: <select name='fechaInicialDay'>";
				$fechaF .= " Dia: <select name='fechaFinalDay'>";
					for( $dia = 1; $dia <= 31; $dia++){
						$fechaI .= "<option value='" . ( $dia < 10 ? "0$dia" : $dia ) . "' " . ($fiDay == $dia ? "selected" : "") . ">$dia</option>";
						$fechaF .= "<option value='" . ( $dia < 10 ? "0$dia" : $dia ) . "' " . ($ffDay == $dia ? "selected" : "") . ">$dia</option>";
					}
				$fechaI .= "</select>";
				$fechaF .= "</select>";
				$fechaI = "<input type='text' id='dpFechaInicial' value='$fiYear-" . ( $fiMonth < 10 ? "0$fiMonth-" : "$fiMonth-" ) . 
																				     ( $fiDay < 10 ? "0$fiDay" : "$fiDay" ) . "' />";
				$fechaF = "<input type='text' id='dpFechaFinal' value='$ffYear-" . ( $ffMonth < 10 ? "0$ffMonth-" : "$ffMonth-" ) . 
																				   ( $ffDay < 10 ? "0$ffDay" : "$ffDay" ) . "' />";
				$resultado = '<div align="center" id="errordial" class="errordial" style="display:' . ($error != '' ? 'block' : 'none' ) . '">' . $error . '</div>
							<br /><br />
							<form action="javascript:editaEvento(document.registrarEvento);" method="post" name="registrarEvento" onsubmit="return validarEvento(this)">
							<table width="500" border="0" class="addInventario">
								<tr><td>Nombre:</td><td width="305"><input name="nombre" type="text" size="40" maxlength="50" value="' . $nombre . '" /></td></tr>
								<tr><td>Fecha Inicial:</td><td width="305">' . $fechaI . '</td></tr>
								<tr><td>Fecha Final:</td><td width="305">' . $fechaF . '</td></tr>
								<tr><td><b>Participantes:</b></td><td></td></tr>' .
								$usuarios .
								'<tr>
									<td colspan="60" align="right"><input name="Guardar" value="Guardar" src="img/guardar.png" type="image">
									<input type="hidden" name="idEvento" value="' . $id . '" />
									<input type="hidden" name="participantes" value="' . $participantes . '" /></td>
								</tr>
							</table>
						</form>';
				echo $resultado;
				break;
			}
			case 'delEvento':{
				global $cnx;
					if(isset($_POST['idEvento'])){
						$idEvento = $_POST['idEvento'];
						$db = getConexion($cnx);
							if($db->conecta()){
								$query = "select count(eventosInventario_id) noRegistros from eventosInventario where origenRegistro <> 'Inventario'and eventos_id = " . $_POST['idEvento'] ;
								$link = $db->db_query($query) or error("Error en la consulta: $query");
								$datos = $db->db_fetch_object($link);
									if($datos->noRegistros == 0){
										$query = "delete from eventosInventario where eventos_id = " . $_POST['idEvento'];
										$db->db_query($query) or error("Error en la consulta: $query");
										$query = "delete from eventosParticipantes where eventos_id = " . $_POST['idEvento'];
										$db->db_query($query) or error("Error en la consulta: $query");
										$query = "delete from eventos where id = " . $_POST['idEvento'];
											if($db->db_query($query)){
												die( '<div align="center" id="errordial" class="errordial" >Evento eliminado correctamente.</div>' );
											}else{
												die( '<div align="center" id="errordial" class="errordial" >Error en la consulta ' . $query . '</div>' );
											}
									}else{
										die( '<div align="center" id="errordial" class="errordial" >Hay registros de inventario capturados por la tablet.<br>No se borrara el evento.</div>' );
									}
							}else{
								die( '<div align="center" id="errordial" class="errordial" >Error de Conexion.</div>' );
							}
					}else{
						die( '<div align="center" id="errordial" class="errordial" >El evento no existe.</div>' );
					}
				break;
			}
			// --------------------------------------- Inventario de Eventos ------------------------------------
			case 'getEventoInventario':{
				$idEvento = parametro( 'idEvento' );
				$buscar = parametro( 'buscar' );
				$buscarCol = parametro( 'buscarCol' );
				$ordenCol = parametro ( 'ordenCol' );
				$ordenDir = parametro ( 'ordenDir' );
				$pagina = parametro ( 'pagina' );
				echo getEventoInventario( $idEvento, $pagina, $buscar, $buscarCol, $ordenCol, $ordenDir);
				break;
			}
			case 'addEventoInventario':{
				global $cnx;
				$error = "";
				$idEvento = parametro( 'idEvento' );
					if(isset($_POST['idDelEvento'])){
						// Agregar el inventario y terminar o mandar error
						$db = getConexion($cnx);
							if($db->conecta()){
								$idEvento = $_POST['idDelEvento'];
								$query = "select max(id) maxId from eventosInventario where eventos_id = $idEvento";
								$link = $db->db_query($query) or error("Error en la consulta: $query");
								$datos = $db->db_fetch_object($link);
								$maxId = $datos->maxId + 1;
								$tipo = $_POST['tipo'];
								$asset = $_POST['asset'];
								$subnumber = $_POST['subnumber'];
								$capitalizedOnY = $_POST['capitalizedOnY'];
								$capitalizedOnM = $_POST['capitalizedOnM'];
								$capitalizedOnD = $_POST['capitalizedOnD'];
								$description = $_POST['description'];
								$acquisVal = $_POST['acquisVal'];
								$accumDep = $_POST['accumDep'];
								$bookVal = $_POST['bookVal'];
								$currency = $_POST['currency'];
								$plant = $_POST['plant'];
								$respCostCenter = $_POST['respCostCenter'];
								$pm = $_POST['pm'];
								$order = $_POST['order'];
								$expiredUsefulLife = $_POST['expiredUsefulLife'];
								$ubicacion = mysql_real_escape_string($_POST['ubicacion']);
								$marca = $_POST['marca'];
								$modelo = $_POST['modelo'];
								$noSerie = $_POST['noSerie'];
								$pedimento = $_POST['pedimento'];
								$statusEtiquetado = $_POST['statusEtiquetado'];
								$statusActivo = $_POST['statusActivo'];
								$origen = $_POST['origen'];
								$comentarios = mysql_real_escape_string($_POST['comentarios']);
								$query = "insert into eventosInventario(eventos_id, id, usuarios_login, origenRegistro, asset, subnumber, capitalizedOn, description, acquisVal, accumDep, bookVal, currency, plant, respCostCenter, `order`, expiredUsefulLife, 
																ubicacion, marca, modelo, noSerie, pedimento, statusEtiquetado, statusActivo, origen, comentarios, pm)values
											($idEvento, $maxId, '" . $_SESSION['usrLogeado'] . "', '$tipo', '$asset', '$subnumber', '$capitalizedOnY-$capitalizedOnM-$capitalizedOnD', '$description', '$acquisVal', '$accumDep', '$bookVal', '$currency', '$plant',
											'$respCostCenter', '$order', '$expiredUsefulLife', '$ubicacion', '$marca', '$modelo', '$noSerie', '$pedimento', '$statusEtiquetado', '$statusActivo',
											'$origen', '$comentarios', '$pm')";
									if($db->db_query($query))
										die ( '<div align="center" id="errordial" class="errordial" >Maquina agregada correctamente</div><script type="text/javascript">actualizar=true;</script>' );
									else{
										$error = "Error en la consulta: $query. Error Mysql: " . mysql_error();
									}
							}else{
								$error = "Problemas de conexion.";
							}
					}
				$anio_hoy = date("Y");
				$fecha = "Año: <select name='capitalizedOnYear'>";
					for( $anio = $anio_hoy - 50; $anio < $anio_hoy; $anio++)
						 $fecha .= "<option value='" . $anio . "'>$anio</option>";
				$fecha .= "</select>";
				$fecha .= " Mes: <select name='capitalizedOnMonth'>";
					for( $mes = 1; $mes <= 12; $mes++)
						$fecha .= "<option value='" . ( $mes < 10 ? "0$mes" : $mes ) . "' " . ( $mes == 1 ? "selected" : "" ) . ">$mes</option>";
				$fecha .= "</select>";
				$fecha .= " Dia: <select name='capitalizedOnDay'>";
					for( $dia = 1; $dia <= 31; $dia++)
						$fecha .= "<option value='" . ( $dia < 10 ? "0$dia" : $dia ) . "' " . ( $dia == 1 ? "selected" : "" ) . ">$dia</option>";
				$fecha .= "</select>";
				$fecha = "<input type='text' id='capitalizedOn' />";
				$resultado = '<div align="center" id="errordial" class="errordial" style="display:' . ($error != '' ? 'block' : 'none' ) . '">' . $error . '</div>
							<br /><br />
							<form action="javascript:agregaEventoInventario(document.registrarInventario);" method="post" name="registrarInventario" onsubmit="return validarEventoInventario(this)">
							<table width="500" border="0" class="addInventario">
								<tr><td>Asset:</td><td width="305"><input name="asset" type="text" size="10" maxlength="20" value="" /></td></tr>
								<tr><td>Subnumber:</td><td width="305"><input name="subnumber" type="text" size="10" maxlength="10" value="" /></td></tr>
								<tr><td>Capitalized On:</td><td width="305">' . $fecha . '</td></tr>
								<tr><td>Description:</td><td width="305"><textarea name="description" cols="30" rows="5"></textarea></td></tr>
								<tr><td>Acquis Val.:</td><td width="305"><input name="acquisVal" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Accum Dep.:</td><td width="305"><input name="accumDep" type="text" size="15" maxlength="100" value="" /></td></tr>
								<tr><td>Book Val.:</td><td width="305"><input name="bookVal" type="text" size="15" maxlength="100" value="" /></td></tr>
								<tr><td>Currency:</td><td width="305"><input name="currency" type="text" size="15" maxlength="100" value="" /></td></tr>
								<tr><td>Plant:</td><td width="305"><input name="plant" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Resp. Cost Center:</td><td width="305"><input name="respCostCenter" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>PM:</td><td width="305"><input name="pm" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Order:</td><td width="305"><input name="order" type="text" size="20" maxlength="100" value="" /></td></tr>
								<tr><td>Expired Useful Life:</td><td width="305"><input name="expiredUsefulLife" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Ubicación:</td><td width="305"><input name="ubicacion" type="text" size="30" maxlength="100" value="" /></td></tr>
								<tr><td>Marca:</td><td width="305"><input name="marca" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Modelo:</td><td width="305"><input name="modelo" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>No Serie:</td><td width="305"><input name="noSerie" type="text" size="25" maxlength="100" value="" /></td></tr>
								<tr><td>Pedimento:</td><td width="305"><input name="pedimento" type="text" size="20" maxlength="100" value="" /></td></tr>
								<tr><td>Tipo:</td><td width="305"><select name="tipo"><option value="Inventario" selected>Inventario</option><option value="Tablet">Tablet</option></select>
								<tr><td>Status de Etiquetado:</td><td width="305"><input name="statusEtiquetado" type="text" size="10" maxlength="100" value="" /></td></tr>
								<tr><td>Status del Activo:</td><td width="305"><select name="statusActivo">
																					<option value="Activo">Activo</option>
																					<option value="Idle operation">Idle Operation</option>
																					<option value="Idle equipment">Idle Equipment</option>
																					<option value="Idle in place">Idle in Place</option>
																					<option value="Scrap">Scrap</option>
																				</select>
																</td></tr>
								<tr><td>Origen:</td><td width="305"><select name="origen">
																					<option value="SAP">SAP</option>
																					<option value="Inv. Interno">Inventario Interno</option>
																				</select>
																</td></tr>
								<tr><td>Comentarios:</td><td width="305"><textarea name="comentarios" cols="30" rows="5"></textarea></td></tr>
								<tr>
									<td colspan="60" align="right">
										<input name="Guardar" value="Guardar" src="img/guardar.png" type="image">
										<input type="hidden" name="idDelEvento" value="' . $idEvento . '">
									</td>
								</tr>
							</table>
						</form>';
				echo $resultado;
				break;
			}
			case 'editEventoInventario':{
				global $cnx;
				$error = "";
				if(isset($_POST['id'])){
					$id = $_POST['id'];
					$db = getConexion($cnx);
						if($db->conecta()){
							$query = "select *, extract(year from capitalizedOn) anio, extract(month from capitalizedOn) mes, extract(day from capitalizedOn) dia 
										from eventosInventario 
										where eventosInventario_id = $id";
							$link = $db->db_query($query) or error("Error en la consulta: $query");
							$datos = $db->db_fetch_object($link);
								if($datos->eventosInventario_id > 0){
									$idEvento = $datos->eventos_id;
									$tipo = $datos->origenRegistro;
									$asset = $datos->asset;
									$subnumber = $datos->subnumber;
									$capitalizedOnY = $datos->anio;
									$capitalizedOnM = $datos->mes;
									$capitalizedOnD = $datos->dia;
									$description = $datos->description;
									$acquisVal = $datos->acquisVal;
									$accumDep = $datos->accumDep;
									$bookVal = $datos->bookVal;
									$currency = $datos->currency;
									$plant = $datos->plant;
									$respCostCenter = $datos->respCostCenter;
									$pm = $datos->pm;
									$order = $datos->order;
									$expiredUsefulLife = $datos->expiredUsefulLife;
									$ubicacion = $datos->ubicacion;
									$marca = $datos->marca;
									$modelo = $datos->modelo;
									$noSerie = $datos->noSerie;
									$pedimento = $datos->pedimento;
									$statusEtiquetado = $datos->statusEtiquetado;
									$statusActivo = $datos->statusActivo;
									$origen = $datos->origen;
									$comentarios = $datos->comentarios;
								}
						}
				}else
					if(isset($_POST['idMaquina'])){
						// Agregar el inventario y terminar o mandar error
						$asset = $_POST['asset'];
						$subnumber = $_POST['subnumber'];
						$capitalizedOnY = $_POST['capitalizedOnY'];
						$capitalizedOnM = $_POST['capitalizedOnM'];
						$capitalizedOnD = $_POST['capitalizedOnD'];
						$description = mysql_real_escape_string($_POST['description']);
						$acquisVal = $_POST['acquisVal'];
						$accumDep = $_POST['accumDep'];
						$bookVal = $_POST['bookVal'];
						$currency = $_POST['currency'];
						$plant = $_POST['plant'];
						$respCostCenter = $_POST['respCostCenter'];
						$pm = $_POST['pm'];
						$order = $_POST['order'];
						$expiredUsefulLife = $_POST['expiredUsefulLife'];
						$ubicacion = $_POST['ubicacion'];
						$marca = $_POST['marca'];
						$modelo = $_POST['modelo'];
						$noSerie = $_POST['noSerie'];
						$pedimento = $_POST['pedimento'];
						$tipo = $_POST['tipo'];
						$statusEtiquetado = $_POST['statusEtiquetado'];
						$statusActivo = $_POST['statusActivo'];
						$origen = $_POST['origen'];
						$comentarios = mysql_real_escape_string($_POST['comentarios']);
						$query = "update eventosInventario 
										set asset = '$asset', subnumber = '$subnumber', capitalizedOn = '$capitalizedOnY-$capitalizedOnM-$capitalizedOnD', description = '$description',
										acquisVal = '$acquisVal', accumDep = '$accumDep', bookVal = '$bookVal', currency = '$currency', plant = '$plant', respCostCenter = '$respCostCenter',
										`order` = '$order', expiredUsefulLife = '$expiredUsefulLife', ubicacion = '$ubicacion', marca = '$marca', modelo = '$modelo', noSerie = '$noSerie',
										pedimento = '$pedimento', statusEtiquetado = '$statusEtiquetado', statusActivo = '$statusActivo', origen = '$origen', comentarios = '$comentarios',
										origenRegistro = '$tipo', pm = '$pm'
									where eventosInventario_id = " . $_POST['idMaquina'];
						$db = getConexion($cnx);
							if($db->conecta()){
								if($db->db_query($query))
									die ( '<div align="center" id="errordial" class="errordial" >Maquina guardada correctamente</div><script type="text/javascript">actualizar=true;</script>' );
								else
									$error = "Error en la consulta: $query. Error Mysql: " . mysql_error();
							}else{
								$error = "Problemas de conexion.";
							}
					}else die ( "No hay accion." );
				$anio_hoy = date("Y");
				$fecha = "Año: <select name='capitalizedOnYear'>";
					for( $anio = $anio_hoy - 50; $anio < $anio_hoy; $anio++)
						 $fecha .= "<option value='" . $anio . "' " . ($capitalizedOnY == $anio ? "selected" : "") . ">$anio</option>";
				$fecha .= "</select>";
				$fecha .= " Mes: <select name='capitalizedOnMonth'>";
					for( $mes = 1; $mes <= 12; $mes++)
						$fecha .= "<option value='" . ( $mes < 10 ? "0$mes" : $mes ) . "' " . ($capitalizedOnM == $mes ? "selected" : "") . ">$mes</option>";
				$fecha .= "</select>";
				$fecha .= " Dia: <select name='capitalizedOnDay'>";
					for( $dia = 1; $dia <= 31; $dia++)
						$fecha .= "<option value='" . ( $dia < 10 ? "0$dia" : $dia ) . "' " . ($capitalizedOnD == $dia ? "selected" : "") . ">$dia</option>";
				$fecha .= "</select>";
				$fecha = "<input type='text' id='capitalizedOn' value='$capitalizedOnY-" . ( $capitalizedOnM < 10 ? "0$capitalizedOnM-" : "$capitalizedOnM-" ) .
																						   ( $capitalizedOnD < 10 ? "0$capitalizedOnD" : "$capitalizedOnD" ) . "' />";
				$resultado = '<div align="center" id="errordial" class="errordial" style="display:' . ($error != '' ? 'block' : 'none' ) . '">' . $error . '</div>
							<br /><br />
							<form action="javascript:editaEventoInventario(document.editarInventario);" method="post" name="editarInventario" onsubmit="return validarEventoInventario(this)">
							<table width="500" border="0" class="addInventario">
								<tr><td>Asset:</td><td width="305"><input name="asset" type="text" size="10" maxlength="20" value="' . $asset . '" /></td></tr>
								<tr><td>Subnumber:</td><td width="305"><input name="subnumber" type="text" size="10" maxlength="10" value="' . $subnumber . '" /></td></tr>
								<tr><td>Capitalized On:</td><td width="305">' . $fecha . '</td></tr>
								<tr><td>Description:</td><td width="305"><textarea name="description" cols="30" rows="5">' . $description . '</textarea></td></tr>
								<tr><td>Acquis Val.:</td><td width="305"><input name="acquisVal" type="text" size="10" maxlength="100" value="' . $acquisVal . '" /></td></tr>
								<tr><td>Accum Dep.:</td><td width="305"><input name="accumDep" type="text" size="15" maxlength="100" value="' . $accumDep . '" /></td></tr>
								<tr><td>Book Val.:</td><td width="305"><input name="bookVal" type="text" size="15" maxlength="100" value="' . $bookVal . '" /></td></tr>
								<tr><td>Currency:</td><td width="305"><input name="currency" type="text" size="15" maxlength="100" value="' . $currency . '" /></td></tr>
								<tr><td>Plant:</td><td width="305"><input name="plant" type="text" size="10" maxlength="100" value="' . $plant . '" /></td></tr>
								<tr><td>Resp. Cost Center:</td><td width="305"><input name="respCostCenter" type="text" size="10" maxlength="100" value="' . $respCostCenter . '" /></td></tr>
								<tr><td>PM:</td><td width="305"><input name="pm" type="text" size="10" maxlength="100" value="' . $pm . '" /></td></tr>
								<tr><td>Order:</td><td width="305"><input name="order" type="text" size="20" maxlength="100" value="' . $order . '" /></td></tr>
								<tr><td>Expired Useful Life:</td><td width="305"><input name="expiredUsefulLife" type="text" size="10" maxlength="100" value="' . $expiredUsefulLife . '" /></td></tr>
								<tr><td>Ubicación:</td><td width="305"><input name="ubicacion" type="text" size="30" maxlength="100" value="' . $ubicacion . '" /></td></tr>
								<tr><td>Marca:</td><td width="305"><input name="marca" type="text" size="10" maxlength="100" value="' . $marca . '" /></td></tr>
								<tr><td>Modelo:</td><td width="305"><input name="modelo" type="text" size="10" maxlength="100" value="' . $modelo . '" /></td></tr>
								<tr><td>No Serie:</td><td width="305"><input name="noSerie" type="text" size="25" maxlength="100" value="' . $noSerie . '" /></td></tr>
								<tr><td>Pedimento:</td><td width="305"><input name="pedimento" type="text" size="20" maxlength="100" value="' . $pedimento . '" /></td></tr>
								<tr><td>Tipo:</td><td width="305"><select name="tipo">
																	<option value="Inventario" ' . ($tipo == "Inventario" ? "selected" : "") . '>Inventario</option>
																	<option value="Tablet" ' . ($tipo == "Tablet" ? "selected" : "") . '>Tablet</option>
																</select>
								<tr><td>Status de Etiquetado:</td><td width="305"><input name="statusEtiquetado" type="text" size="10" maxlength="100" value="' . $statusEtiquetado . '" /></td></tr>
								<tr><td>Status del Activo:</td><td width="305"><select name="statusActivo">
																					<option value="Activo" ' . ($statusActivo == "Activo" ? "selected" : "" ) . '>Activo</option>
																					<option value="Idle operation" ' . ($statusActivo == "Idle operation" ? "selected" : "" ) . '>Idle Operation</option>
																					<option value="Idle equipment" ' . ($statusActivo == "Idle equipment" ? "selected" : "" ) . '>Idle Equipment</option>
																					<option value="Idle in place" ' . ($statusActivo == "Idle in place" ? "selected" : "" ) . '>Idle in Place</option>
																					<option value="Scrap" ' . ($statusActivo == "Scrap" ? "selected" : "" ) . '>Scrap</option>
																				</select>
																</td></tr>
								<tr><td>Origen:</td><td width="305"><select name="origen">
																					<option value="SAP" ' . ($origen == "SAP" ? "selected" : "" ) . '>SAP</option>
																					<option value="Inv. Interno" ' . ($origen == "Inv. Interno" ? "selected" : "" ) . '>Inventario Interno</option>
																				</select>
																</td></tr>
								<tr><td>Comentarios:</td><td width="305"><textarea name="comentarios" cols="30" rows="5">' . $comentarios . '</textarea></td></tr>
								<tr>
									<td colspan="60" align="right"><input name="Guardar" value="Guardar" src="img/guardar.png" type="image">
									<input type="hidden" name="idMaquina" value="' . $id . '">
									<input type="hidden" name="idEvento" value="' . $idEvento . '"</td>
								</tr>
							</table>
						</form>';
				echo $resultado;
				break;
			}
			case 'delEventoInventario':{
				global $cnx;
					if(isset($_POST['idMaquina'])){
						$id = $_POST['idMaquina'];
						$db = getConexion($cnx);
							if($db->conecta()){
								$query = "delete from eventosInventario where eventosInventario_id = $id";
									if($db->db_query($query)){
										die( '<div align="center" id="errordial" class="errordial" >Maquina eliminada correctamente.</div>' );
									}else{
										die( '<div align="center" id="errordial" class="errordial" >Error en la consulta ' . $query . '</div>' );
									}
							}else{
								die( '<div align="center" id="errordial" class="errordial" >Error de Conexion.</div>' );
							}
					}else{
						die( '<div align="center" id="errordial" class="errordial" >La maquina no existe.</div>' );
					}
				break;
			}
			//-------------------------------------------- Servicios --------------------------------------
			case 'logea':{
				global $cnx;
				$login = parametro('login');
				$pass = parametro('pass');
					if(logea($login, $pass, true)){
						$db = getConexion($cnx);
							if($db->conecta()){
								$query = "select nombre, email from usuarios where login = '$login'";
								$link = $db->db_query($query) or error( "Error en la consulta: $query" );
								$datos = $db->db_fetch_object($link);
								$nombre = $datos->nombre;
								$email = $datos->email;
								$eventos = array();
								$query = "select * from eventos
											where id in ( select eventos_id from eventosParticipantes where usuarios_login = '$login')  and fechaInicial <= now() and fechaFinal >= now()";
								$link = $db->db_query($query) or error("Error en la consulta: $query");
									while($datos = $db->db_fetch_object($link)){
										$inventario = array();
										$inventarioTablet = array();
										$foto = "";
										$query = "select * from eventosInventario where eventos_id = {$datos->id} and subnumber = '0'";
                                        //echo $query;
										$link2 = $db->db_query($query) or error("Error en la consulta: $query");
											while( $datos2 = $db->db_fetch_object($link2)){
												$foto = $datos2->foto;
												if($datos2->origenRegistro == 'Inventario'){
														if ( $datos2->foto == "" ){
															$nombreImagen = $datos2->asset . ".jpg";
															$path = '.' . DS . "imgInventario" . DS . $nombreImagen;
																if( file_exists( $path ) ){
																	$foto = "http://" . $_SERVER["HTTP_HOST"] . DS . "Inventarios" . DS . "imgInventario" . DS . $nombreImagen;
																	$query = "update inventario set foto = '$foto' where asset = '{$datos2->asset}'";
																	$db->db_query( $query ) or die ( '{"exito":0,"error":"Error en la consulta: ' . $query . '; ' . mysql_error() . '}' );
																}
														}
													$inventario[] = array(
														"asset"=>$datos2->asset,
														"description"=>$datos2->description,
														"ubicacion"=>$datos2->ubicacion,
														"marca"=>$datos2->marca,
														"modelo"=>$datos2->modelo,
														"noSerie"=>$datos2->noSerie,
														"pedimento"=>$datos2->pedimento,
														"statusEtiquetado"=>$datos2->statusEtiquetado,
														"statusActivo"=>$datos2->statusActivo,
														"origen"=>$datos2->origen,
														"respCostCenter"=>$datos2->respCostCenter,
														"pm"=>$datos2->pm,
														"foto"=>$foto
													);
												}else{
														if ( $datos2->foto == "" ){
															$nombreImagen = $datos2->asset . ".jpg";
															$path = '.' . DS . "imgInventario" . DS . $datos->id . DS . $nombreImagen;
																if( file_exists( $path ) ){
																	$foto = "http://" . $_SERVER["HTTP_HOST"] . DS . "Inventarios" . DS . "imgInventario" . DS . $datos->id . DS . $nombreImagen;
																	$query = "update eventosInventario set foto = '$foto' where eventosInventario_id = {$datos->id}";
																	$db->db_query( $query ) or die ( '{"exito":0,"error":"Error en la consulta: ' . $query . '; ' . mysql_error() . '}' );
																}
														}
													$inventarioTablet[] = array(
														"asset"=>$datos2->asset,
														"description"=>$datos2->description,
														"ubicacion"=>$datos2->ubicacion,
														"marca"=>$datos2->marca,
														"modelo"=>$datos2->modelo,
														"noSerie"=>$datos2->noSerie,
														"pedimento"=>$datos2->pedimento,
														"statusEtiquetado"=>$datos2->statusEtiquetado,
														"statusActivo"=>$datos2->statusActivo,
														"origen"=>$datos2->origen,
														"comentarios"=>$datos2->comentarios,
														"respCostCenter"=>$datos2->respCostCenter,
														"pm"=>$datos2->pm,
														"foto"=>$foto
													);
												}
											}
										$eventos[] = array(
											"id"=>$datos->id,
											"nombre"=>$datos->nombre,
											"fechaInicial"=>$datos->fechaInicial,
											"fechaFinal"=>$datos->fechaFinal,
											"inventario"=>$inventario,
											"inventarioTablet"=>$inventarioTablet
										);
									}
								$respuesta['exito'] = 1;
								$respuesta['nombre'] = $nombre;
								$respuesta['email'] = $email;
								$respuesta['eventos'] = $eventos;
							}else{
								$respuesta['exito'] = 0;
								$respuesta['error'] = "Error al conectarse al servidor de base de datos.";
							}
					}else{
						$respuesta['exito'] = 0;
						$respuesta['error'] = "Usuario o contraseña no son correctos.";
					}
				//echo formatData($respuesta);
				echo formatData( $respuesta, SALIDA_SERVICIOS );
				break;
			}
			case 'addMaquinaInventario':{
				$idEvento = parametro('idEvento');
				$usuario = parametro ('usuario');
				$asset = parametro ('asset');
				$ubicacion = mysql_real_escape_string( parametro ('ubicacion') );
				$marca = parametro ('marca');
				$modelo = parametro ('modelo');
				$noSerie = parametro ('noSerie');
				$pedimento = parametro ('pedimento');
				$statusEtiquetado = parametro ('statusEtiquetado');
				$statusActivo = parametro ('statusActivo');
				$comentarios = mysql_real_escape_string(parametro ('comentarios'));
				$pm = mysql_real_escape_string(parametro('pm'));
				$origen = parametro('origen');
					if( $asset != "" && $usuario != "" && $idEvento != "" ){
						global $cnx;
						$db = getConexion($cnx);
							if($db->conecta()){
								$query = "select count(eventosInventario_id) noRegistros from eventosInventario 
											where eventos_id = $idEvento and asset = '$asset' and subnumber = '0' and origenRegistro = 'Tablet'";
								$link = $db->db_query($query) or error("Error en la consulta: $query");
								$datos = $db->db_fetch_object($link);
									if($datos->noRegistros == 0){
										$query = "select * from eventosInventario where eventos_id = $idEvento and asset = '$asset' and subnumber = '0'";
										$link = $db->db_query($query) or error("Error en la consulta: $query");
										$datos = $db->db_fetch_object($link);
											if(isset($datos->asset)){
												$capitalizedOn = $datos->capitalizedOn;
												$description = mysql_real_escape_string($datos->description);
												$acquisVal = $datos->acquisVal;
												$accumDep = $datos->accumDep;
												$bookVal = $datos->bookVal;
												$currency = $datos->currency;
												$plant = $datos->plant;
												$respCostCenter = $datos->respCostCenter;
												$order = $datos->order;
												$expiredUsefulLife = $datos->expiredUsefulLife;
											}
										$acquisVal = ( $acquisVal == "" ? "0" : $acquisVal );
										$accumDep = ( $accumDep == "" ? "0" : $accumDep );
										$bookVal = ( $bookVal == "" ? "0" : $bookVal );
										$query = "insert into eventosInventario(eventos_id, usuarios_login, origenRegistro, asset, subnumber, capitalizedOn, description,
													acquisVal, accumDep, bookVal, currency, plant, respCostCenter, `order`, expiredUsefulLife, ubicacion, marca, modelo,
													noSerie, pedimento, statusEtiquetado, statusActivo, origen, comentarios, pm)
													values($idEvento, '$usuario', 'Tablet', '$asset', '0', " . ( $capitalizedOn == "" ? "now()" : "'$capitalizedOn'" ) . 
													", '$description', $acquisVal, $accumDep, $bookVal, '$currency', '$plant', '$respCostCenter', '$order', '$expiredUsefulLife',
													'$ubicacion', '$marca', '$modelo', '$noSerie', '$pedimento', '$statusEtiquetado', '$statusActivo', '$origen', '$comentarios',
													'$pm')";
											if($db->db_query($query)){
												$idAgregado = mysql_insert_id();
												if ( isset ( $_FILES['foto'] ) )
													if ( $_FILES['foto']['error'] == 0 ){
														$file_opc = pathinfo ( $_FILES['foto']['name'] );
															if ( in_array ( strtolower ( $file_opc['extension'] ), array ( 'jpg', 'jpeg', 'png', 'gif' ) ) ){
																//$nombreImagen = $_SERVER["HTTP_HOST"] . DS . $file_opc . ;
																$nombreImagen = $asset . "." . $file_opc['extension'];
																$folder = "." . DS . "imgInventario" . DS;
																	if (!file_exists($folder)) 
																		mkdir($folder, 0777);
																chmod($folder,  0777);
																move_uploaded_file($_FILES['foto']['tmp_name'], $folder . $nombreImagen );
																$fotoPath = "http://" . $_SERVER["HTTP_HOST"] . DS . "Inventarios" . DS . "imgInventario" . DS . $nombreImagen;
																$query = "update eventosInventario set foto = '$fotoPath' where eventosInventario_id = $idAgregado";
																$db->db_query( $query ) or die ( '{"exito":0,"error":"Error en la consulta: ' . $query . '; ' . mysql_error() . '}' );
																//$fotoEmpleado = str_replace( DS, "/", $fotoEmpleado );
															}
													}
												$respuesta['exito'] = 1;
											}else{
												$respuesta['exito'] = 0;
												$respuesta['error'] = "Error en la consulta: $query, mysqlError: " . mysql_error();
											}
									}else{
										$respuesta['exito'] = 0;
										$respuesta['error'] = "El asset ($asset) ya fue registrado.";
									}
							}else{
								$respuesta['exito'] = 0;
								$respuesta['error'] = "Error de conexion.";
							}
					}else{
						$respuesta['exito'] = 0;
						$respuesta['error'] = "Los campos asset, usuario y id del evento no puede estar vacio.";
					}
				echo formatData($respuesta, SALIDA_SERVICIOS);
				break;
			}
			default:{
				echo "No se encontro la opcion";
			}
		}
	}
?>