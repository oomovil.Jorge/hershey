<?php
/*
	DataBase
	Nota:
	Para desactivar las funciones de SqlServer (si no esta instalada la libreria va a mandar errores por estas funciones)
	desactivar reemplaza la cadena /^^^/    por  /^^ini/
	activar    reemplaza la cadena /^^ini/  por  /^^^/
	^ = *
*/

class db{
	const MYSQL     = "MYSQL";
	const SQLSERVER = "SQLSERVER";
	private $tipoDB, $servidor, $usuario, $password, $nombreDB;
	private $infoConexion;
	private $sqlserverConexion;

	// Constructor
	function db($tipoDB, $servidor, $usuario, $password, $nombreDB){
		$this->tipoDB = $tipoDB;
		$this->servidor = $servidor;
		$this->usuario = $usuario;
		$this->password = $password;
		$this->nombreDB = $nombreDB;
			if ( $this->tipoDB == db::SQLSERVER ){
				$this->infoConexion["UID"] = $this->usuario;
				$this->infoConexion["PWD"] = $this->password;
				$this->infoConexion["Database"] = $this->nombreDB;
			}
	}
	
	// Funcion para hacer la conexion de la base de datos.
	function conecta(){
			switch ( $this->tipoDB ){
				case db::MYSQL:{
					$link = @mysql_connect($this->servidor, $this->usuario, $this->password) or die("MySQL=Conecta() Error: ". mysql_error());
					@mysql_select_db( $this->nombreDB, $link );
					break;
				}
				/***/
				case db::SQLSERVER:{
					$this->sqlserverConexion = sqlsrv_connect( $this->servidor, $this->infoConexion);
						if ( !$this->sqlserverConexion ){
							echo "SqlServer=Conecta() Error: ";
							die( print_r( sqlsrv_errors(), true ) );
						}
					break;
				}
				/**/
			}
		return true;
	}
	
	// db_query
	function db_query($query){
			switch ( $this->tipoDB ){
				case db::MYSQL:{
					return mysql_query($query);
					break;
				}
				/***/
				case db::SQLSERVER:{
					return sqlsrv_query ( $this->sqlserverConexion, $query );
					break;
				}
				/**/
			}
	}
	
	// db_fetch_object
	function db_fetch_object($resultado){
			switch ( $this->tipoDB ){
				case db::MYSQL:{
					return mysql_fetch_object($resultado);
					break;
				}
				/***/
				case db::SQLSERVER:{
					return sqlsrv_fetch_object($resultado);
					break;
				}
				/**/
			}
	}
	
	// db_fetch_assoc
	function db_fetch_assoc($resultado){
			switch ( $this->tipoDB ){
				case db::MYSQL:{
					return mysql_fetch_assoc($resultado);
					break;
				}
				/***/
				case db::SQLSERVER:{
					//return sqlsrv_fetch_object($resultado);
					break;
				}
				/**/
			}	
	}
	
	// destructor
	function __destruct(){
			switch ( $this->tipoDB ){
				case db::MYSQL:{
					//mysql_close();
					break;
				}
				/***/
				case db::SQLSERVER:{
					if ( isset ($this->sqlserverConexion)) sqlsrv_close($this->sqlserverConexion);
					break;
				}
				/**/
			}	
	}
	
	// db_num_rows
	function db_num_rows($resultado, $query = ""){
			switch ( $this->tipoDB ){
				case db::MYSQL:{
					return mysql_num_rows($resultado);
					break;
				}
				/***/
				case db::SQLSERVER:{
					$parametros = array();
					$opciones = array( "Scrollable"=>SQLSRV_CURSOR_KEYSET );
					$stmt = sqlsrv_query( $this->sqlserverConexion, $query, $parametros, $opciones );
					$row_count = sqlsrv_num_rows($stmt);
						if ($row_count === false)
						   echo "Error in retrieveing row count. query: $query: " .  print_r( sqlsrv_errors(), true );
						else
						   return $row_count;	
					//return sqlsrv_num_rows($stmt);
					break;
				}
				/**/
			}
	}
}
?>