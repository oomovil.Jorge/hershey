<?php 
	session_start(); 
	include ( "funciones.php" );
	$errorLogeo = "";
		if(isset($_POST['login'])){
			if(logea( $_POST['login'], $_POST['pass'])){
				$_SESSION['usrLogeado'] = $_POST['login'];
				$_SESSION['usrNombre'] = getNombreUsuario( $_POST['login'] );
			}else{
				$errorLogeo = "Usuario y/o contraseña incorrecta.";
			}
		}
	$usuarioLogeado = "";
		if(isset($_SESSION['usrLogeado'])){
			//$_SESSION['usrLogeado'] = 'CaRLoS';
			header ('location: inventario.php');
		}
	
	global $cnx, $contenido_main;
		if($contenido_main == "" && !isset($_SESSION['usrLogeado'])){
			$contenido_main = '<p style="width:100%; text-align:left"><font size="+3">INICIO DE SESION</font></p>
                    <div class="barraInferior"></div>
                    <div>
                    	<table>
                        	<tr><td align="left">
                            <form name="frmLogeo" class="frmLogeo" action="index.php" method="post">
                            	<p style="margin-top:30px">
                                    <font size="+2">USUARIO:</font><br/>
                                    <input type="text" name="login" onkeyup="intro(event, \'pass\')"/><br />
                                </p>
                                <p style="margin-top:50px">
                                    <font size="+2">CONTRASEÑA:</font><br />
                                    <input type="password" name="pass" id="pass" onkeyup="Accede(event)" />
                                </p>
                                <p style="margin-top:50px;" align="center">
                                    <a href="#"><img src="img/btnEntrar.png" onclick="frmLogeo.submit();" /></a>
                                </p>
								<div class="divError" align="center" style="display:" ' . ( $errorLogeo == "" ? 'none' : 'block' ) . '>' . $errorLogeo . '</div>
                                <p align="center">
                                	<a href="#" class="link">¿OLVIDASTE TU CONTRASEÑA?</a>
                                </p>
                            </form>
                            </td></tr>
                    	</table>
					</div>';
		}
	date_default_timezone_set('America/Los_Angeles');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Hersheys Inventarios</title>
</head>
<link href="main.css" rel="stylesheet" type="text/css" />
<link href="jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="jquery-ui.js"></script>
<script type="text/javascript">
	function intro(evt, next_id) {
    	switch (evt.keyCode) {
			case 37: //left arrow
			case 39: //right arrow
			case 33: //page up
			case 34: //page down
			case 36: //home
			case 35: //end
				break;
			case 13: //enter
				document.getElementById(next_id).focus();
				break;
			case 9: //tab
			case 27: //esc
			case 16: //shift
			case 17: //ctrl
			case 18: //alt
			case 20: //caps lock
			case 8: //backspace
			case 46: //delete
			case 38: //up arrow
			case 40: //down arrow
                break;
		}
        if (evt.keyCode == undefined)
            document.getElementById(next_id).focus();
    }
	
    function Accede(evt) {
        switch (evt.keyCode) {
            case 13: //enter
                document.forms.frmLogeo.submit();
                break;
        }
    }
</script>
<body>
	<div id="container">
    	<table class="tPrincipal" border="0" cellspacing="0" cellpadding="0">
        	<tr id="trHeader"><td colspan="3">
            	<div class="barraSup1" style="height:24px;" align="right">
                	<?php echo $usuarioLogeado; ?>
                </div>
                <div class="barraSup2" style="height:74px;" align="center">
                	<p><img src="img/titulo.png" style="margin-top:15px"/></p>
                </div>
            </td></tr>
            <tr id="trCuerpo"><td width="400px"><img src="img/fondoIzqCentro.png" /></td><td>
                <div class="mainInfo" align="center" style="height:auto">
                	<?php echo $contenido_main; ?>
                </div>
            </td><td width="400px"><img src="img/fondoDerCentro.png" /></td></tr>
            <tr id="trPie"><td colspan="3">
                <div class="pie" style="vertical-align:bottom">
                  <p style="color:#FFF; margin-top:18px">&copy; Hersheys de Mexico 2014</p>
                </div>
            </td></tr>
        </table>
    </div>
</body>
</html>