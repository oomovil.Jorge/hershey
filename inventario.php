<?php
	session_start();
	include ( 'funciones.php' );
		if(!isset($_SESSION['usrLogeado']))
			header( 'location: index.php' );
		if( isset( $_FILES['archivoInventario'] ) ){
			if($_FILES['archivoInventario']['error']==0){
				$file_opc = pathinfo($_FILES [ 'archivoInventario' ][ 'name' ]);
					if (strtoupper($file_opc [ 'extension' ] ) == 'CSV'){
						$script = "";
						$arrayScript = array();
						$lines = @file($_FILES [ 'archivoInventario' ][ 'tmp_name' ]);
							for ($x = 1; $x < count ($lines); $x++){
									if($script == '' || $x % 100 == 0){
										$insert = "insert into inventario(asset, subnumber, capitalizedOn, description, acquisVal, accumDep, bookVal, currency, plant, respCostCenter, 
																					`order`, expiredUsefulLife, ubicacion, marca, modelo, noSerie, pedimento, foto, statusEtiquetado, statusActivo, 
																					origen, comentarios, pm)values";
											if($script == "")
												$script = $insert;
											else
												$script = substr($script, 0, -1) . ";" . $insert;
										//$script .= ( $script == '' ? $insert : substr($script, 0, -1) . ";$insert" );
									}
								$lineaFix = ParseaCVS($lines[$x]);
								$tmp = explode (',', $lineaFix);
								$asset = ''; $subnumber = ''; $capitalizedOn = ''; $description = ''; $acquisVal = ''; $accumDep = ''; $bookVal = ''; $currency = ''; $plant = '';
								$respCostCenter = ''; $order = ''; $expiredUsefulLife = ''; $ubicacion = ''; $marca = ''; $modelo = ''; $noSerie = ''; $pedimento = ''; $foto = ''; 
								$statusEtiquetado = ''; $statusActivo = ''; $origen = ''; $comentarios = ''; $pm = '';
									for($y = 0; $y < count($tmp); $y++){
										if($y ==  0) $asset = ArreglaCampo($tmp[$y]);             else
										if($y ==  1) $subnumber = ArreglaCampo($tmp[$y]);         else
										if($y ==  2) $capitalizedOn = ArreglaCampo($tmp[$y]);     else
										if($y ==  3) $description = ArreglaCampo($tmp[$y]);       else
										if($y ==  4) $acquisVal = ArreglaCampo($tmp[$y]);         else
										if($y ==  5) $accumDep = ArreglaCampo($tmp[$y]);          else
										if($y ==  6) $bookVal = ArreglaCampo($tmp[$y]);           else
										if($y ==  7) $currency = ArreglaCampo($tmp[$y]);          else
										if($y ==  8) $plant = ArreglaCampo($tmp[$y]);             else
										if($y ==  9) $respCostCenter = ArreglaCampo($tmp[$y]);    else
										if($y == 10) $pm = ArreglaCampo($tmp[$y]);			      else
										if($y == 11) $order = ArreglaCampo($tmp[$y]);        	  else 
										if($y == 12) $expiredUsefulLife = ArreglaCampo($tmp[$y]); else
										if($y == 13) $ubicacion = ArreglaCampo($tmp[$y]);         else 
										if($y == 14) $marca = ArreglaCampo($tmp[$y]);        	  else 
										if($y == 15) $modelo = ArreglaCampo($tmp[$y]);            else
										if($y == 16) $noSerie = ArreglaCampo($tmp[$y]);           else 
										if($y == 17) $pedimento = ArreglaCampo($tmp[$y]);         else 
										if($y == 18) $foto = ArreglaCampo($tmp[$y]);              else
										if($y == 19) $statusEtiquetado = ArreglaCampo($tmp[$y]);  else 
										if($y == 20) $statusActivo = ArreglaCampo($tmp[$y]);      else 
										if($y == 21) $origen = ArreglaCampo($tmp[$y]);            else
										if($y == 22) $comentarios = ArreglaCampo($tmp[$y]);
									}
									if($capitalizedOn != ""){
                                        if(strpos($capitalizedOn, " de ") !== false){
                                            $dia = substr($capitalizedOn, 0, 2);
                                            $mesLetra = substr($capitalizedOn, 6);
                                            $mesLetra = substr($mesLetra, 0, strpos($mesLetra, " "));
                                            $mesLetra = strtoupper($mesLetra);
                                                if($mesLetra == "ENERO")      $mes = "01"; else
                                                if($mesLetra == "FEBRERO")    $mes = "02"; else
                                                if($mesLetra == "MARZO")      $mes = "03"; else
                                                if($mesLetra == "ABRIL")      $mes = "04"; else
                                                if($mesLetra == "MAYO")       $mes = "05"; else
                                                if($mesLetra == "JUNIO")      $mes = "06"; else
                                                if($mesLetra == "JULIO")      $mes = "07"; else
                                                if($mesLetra == "AGOSTO")     $mes = "08"; else
                                                if($mesLetra == "SEPTIEMBRE") $mes = "09"; else
                                                if($mesLetra == "OCTUBRE")    $mes = "10"; else
                                                if($mesLetra == "NOVIEMBRE")  $mes = "11"; else $mes = "12";
                                            $anio = substr($capitalizedOn, strlen($capitalizedOn) - 4);
                                        }else{
                                            $dia = substr($capitalizedOn, 0, 2);
                                            $mes = substr($capitalizedOn, 3, 2);
                                            $anio = substr($capitalizedOn, 6, 4);
                                        }
										$capitalizedOn = "$anio-$mes-$dia";
									}
								$acquisVal = ( $acquisVal == '' ? '0.0' : str_replace(',', '', $acquisVal));
								$accumDep = ( $accumDep == '' ? '0.0' : str_replace(',', '', $accumDep));
								$bookVal = ( $bookVal == '' ? '0.0' : str_replace(',', '', $bookVal));
								$expiredUsefulLife = ( $expiredUsefulLife == '' ? '0' : str_replace(',', '', $expiredUsefulLife ));
								$insert = "('$asset', '$subnumber', '$capitalizedOn', '$description', $acquisVal, $accumDep, $bookVal, '$currency', '$plant', '$respCostCenter', 
											'$order', $expiredUsefulLife, '$ubicacion', '$marca', '$modelo', '$noSerie', '$pedimento', '$foto', '$statusEtiquetado', '$statusActivo', 
											'$origen', '$comentarios', '$pm'),";
								$script .= $insert;
								//
								$insert = "insert into inventario(asset, subnumber, capitalizedOn, description, acquisVal, accumDep, bookVal, currency, plant, respCostCenter, 
																`order`, expiredUsefulLife, ubicacion, marca, modelo, noSerie, pedimento, foto, statusEtiquetado, statusActivo, 
																origen, comentarios, pm)values('$asset', '$subnumber', '$capitalizedOn', '$description', $acquisVal, $accumDep, $bookVal, '$currency', '$plant', '$respCostCenter', 
											'$order', $expiredUsefulLife, '$ubicacion', '$marca', '$modelo', '$noSerie', '$pedimento', '$foto', '$statusEtiquetado', '$statusActivo', 
											'$origen', '$comentarios', '$pm');";
								$arrayScript[] = $insert;
							}
						$errorScript = "";
						$db = getConexion($cnx);
							if($db->conecta() && count($arrayScript) > 0){
								$db->db_query("delete from inventario");
								for($x = 0; $x < count($arrayScript); $x++)
									if(!@mysql_query($arrayScript[$x]))
										$errorScript .= "Error en la importacion, linea: " . ($x - 1);
							}
					}
			}
		}
	//$usuarioLogeado = '<font color="#FFFF00">' . $_SESSION['usrNombre'] . "</font> <a href='logout.php' class='link1'>Cerrar sesión</a>";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Hersheys Inventarios</title>
</head>
<link href="main.css" rel="stylesheet" type="text/css" />
<link href="jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="jquery-ui.js"></script>
<script>
	function mostrarFiltro(id){
		if ( $( "#" + id ).is ( ':visible' ) ) {
			$( "#" + id ).hide ( 'slow' );
		}else{
			$( "#" + id ).show ( 'slow' )
		}
	}

	function getInventario(pagina){
		vbuscar = document.getElementById('buscar').value;
		buscarCol = document.getElementById('sColBuscar').value;
		ordenCol = document.getElementById('sColOrdenar').value;
		ordenDir = document.getElementById('sOrdeDireccion').value;
		$.ajax({
			type: "POST",
			url: 'funciones.php',
			data:'cmd=getInventario&buscar=' + vbuscar + '&buscarCol=' + buscarCol + '&ordenCol=' + ordenCol + '&ordenDir=' + ordenDir + '&pagina=' + pagina,
			success: function(data){
				$('#divDatosInventario').html( data );	
			},
			beforeSend :function() {
		  		$('#divDatosInventario').html('<div style="width=100%" align="center"><img src="loader.gif" width="66" height="66" alt="Cargando" /></div>');
	  		}
		});
	}
	
	function addInventario(){
		title = 'Agregar Inventario';
		h = 900;
		w = 600;
		$.ajax({
			type: "POST",
		  	url: 'funciones.php',
		  	data:'cmd=addInventario',
		  	success: function ( data ){
					if ( data == -1 ){
						alert ( 'Su sesión a expirado' );
						window.location = 'logout.php';
					}else{
						$('.cajas_menu_cont').html(data);
						calendario();
					}
		  	},
		  	beforeSend :function() {
			  	actualizar = false;
				$("body").append ( '<div class="cat"><div class="cajas_menu_cont" id="cajas_menu_cont"><div style="margin-top:36px; witdh=90%" align="center" >' +
									'<img src="loader.gif" width="66" height="66" alt="Cargando" /></div></div></div>');
				$('.cat').dialog({ modal:true,
					closeText: 'Cerrar',
					title: title,
					height: h,
					width: w,
					close: function(){ //indicamos la función que se ejecutará al cerrarse la ventana
						$(this).remove(); //borramos la capa
							if ( actualizar == true ){
								actualizar = false;
								//location.reload ( true );
								getInventario(0);
							}
					}
				});
		  	}
		});
	}
	
	function validarInventario(obj){
		if (obj.asset.value.length == 0) {
				document.getElementById('errordial').innerHTML='El campo Asset no puede estar vacio.';
				$("#errordial").slideDown('slow')
				return false;
		}else{
			return true;
		}
	}
	
	function agregaInventario(obj){
		vAsset = obj.asset.value;
        /* MODIFICACION KUFF */
//		vSubnumber = obj.asset.value;
        vSubnumber = obj.subnumber.value;
        /* FIN MODIFICACION KUFF */
		vCapitalizedOnYear = obj.capitalizedOn.value.substr(0, 4);
		vCapitalizedOnMonth = obj.capitalizedOn.value.substr(5, 2);
		vCapitalizedOnDay = obj.capitalizedOn.value.substr(8, 2);
		vDescription = obj.description.value;
		vAcquisVal = obj.acquisVal.value;
		vAccumDep = obj.accumDep.value;
		vBookVal = obj.bookVal.value;
		vCurrency = obj.currency.value;
		vPlant = obj.plant.value;
		vRespCostCenter = obj.respCostCenter.value;
		vOrder = obj.order.value;
		vExpiredUsefulLife = obj.expiredUsefulLife.value;
		vUbicacion = obj.ubicacion.value;
		vMarca = obj.marca.value;
		vModelo = obj.modelo.value;
		vNoSerie = obj.noSerie.value;
		vPedimento = obj.pedimento.value;
		vStatusEtiquetado = obj.statusEtiquetado.value;
		vStatusActivo = obj.statusActivo.value;
		vOrigen = obj.origen.value;
		vComentarios = obj.comentarios.value;
		vPm = obj.pm.value;
		$.ajax({
			type: "POST",
			url: 'funciones.php',
			data:'cmd=addInventario&asset=' + vAsset + '&subnumber=' + vSubnumber + '&capitalizedOnY=' + vCapitalizedOnYear + '&capitalizedOnM=' + vCapitalizedOnMonth + '&capitalizedOnD=' + vCapitalizedOnDay +
				'&description=' + vDescription + '&acquisVal=' + vAcquisVal + '&accumDep=' + vAccumDep + '&bookVal=' + vBookVal + '&currency=' + vCurrency + '&plant=' + vPlant + '&respCostCenter=' + vRespCostCenter +
				'&order=' + vOrder + '&expiredUsefulLife=' + vExpiredUsefulLife + '&ubicacion=' + vUbicacion + '&marca=' + vMarca + '&modelo=' + vModelo + '&noSerie=' + vNoSerie + '&pedimento=' + vPedimento +
				'&statusEtiquetado=' + vStatusEtiquetado + '&statusActivo=' + vStatusActivo + '&origen=' + vOrigen + '&pm=' + vPm + '&comentarios=' + vComentarios,
			success: function(data){
				$('#cajas_menu_cont').html( data );	
			},
			beforeSend :function() {
		  		$('#cajas_menu_cont').html('<div style="width=100%" align="center"><img src="loader.gif" width="66" height="66" alt="Cargando" /></div>');
	  		}
		});
	}
	
	function editar(idMaquina){
		title = 'Editar Maquina';
		h = 900;
		w = 600;
		$.ajax({
			type: "POST",
		  	url: 'funciones.php',
		  	data:'cmd=editInventario&id=' + idMaquina,
		  	success: function ( data ){
					if ( data == -1 ){
						alert ( 'Su sesión a expirado' );
						window.location = 'logout.php';
					}else{
						$('.cajas_menu_cont').html(data);
						calendario();
					}
		  	},
		  	beforeSend :function() {
			  	actualizar = false;
				$("body").append ( '<div class="cat"><div class="cajas_menu_cont" id="cajas_menu_cont"><div style="margin-top:36px; witdh=90%" align="center" >' +
									'<img src="loader.gif" width="66" height="66" alt="Cargando" /></div></div></div>');
				$('.cat').dialog({ modal:true,
					closeText: 'Cerrar',
					title: title,
					height: h,
					width: w,
					close: function(){ //indicamos la función que se ejecutará al cerrarse la ventana
						$(this).remove(); //borramos la capa
							if ( actualizar == true ){
								actualizar = false;
								//location.reload ( true );
								getInventario(0);
							}
					}
				});
		  	}
		});	
	}
	
	function editaInventario(obj){
		vAsset = obj.asset.value;
        /* MODIFICACION KUFF */
//		vSubnumber = obj.asset.value;
        vSubnumber = obj.subnumber.value;
        /* FIN MODIFICACION KUFF */
		vCapitalizedOnYear = obj.capitalizedOn.value.substr(0, 4);
		vCapitalizedOnMonth = obj.capitalizedOn.value.substr(5, 2);
		vCapitalizedOnDay = obj.capitalizedOn.value.substr(8, 2);
		vDescription = obj.description.value;
		vAcquisVal = obj.acquisVal.value;
		vAccumDep = obj.accumDep.value;
		vBookVal = obj.bookVal.value;
		vCurrency = obj.currency.value;
		vPlant = obj.plant.value;
		vRespCostCenter = obj.respCostCenter.value;
		vOrder = obj.order.value;
		vExpiredUsefulLife = obj.expiredUsefulLife.value;
		vUbicacion = obj.ubicacion.value;
		vMarca = obj.marca.value;
		vModelo = obj.modelo.value;
		vNoSerie = obj.noSerie.value;
		vPedimento = obj.pedimento.value;
		vStatusEtiquetado = obj.statusEtiquetado.value;
		vStatusActivo = obj.statusActivo.value;
		vOrigen = obj.origen.value;
		vComentarios = obj.comentarios.value;
		vPm = obj.pm.value;
		$.ajax({
			type: "POST",
			url: 'funciones.php',
			data:'cmd=editInventario&idMaquina=' + obj.idMaquina.value + '&asset=' + vAsset + '&subnumber=' + vSubnumber + '&capitalizedOnY=' + vCapitalizedOnYear + '&capitalizedOnM=' + vCapitalizedOnMonth + '&capitalizedOnD=' + vCapitalizedOnDay +
				'&description=' + vDescription + '&acquisVal=' + vAcquisVal + '&accumDep=' + vAccumDep + '&bookVal=' + vBookVal + '&currency=' + vCurrency + '&plant=' + vPlant + '&respCostCenter=' + vRespCostCenter +
				'&order=' + vOrder + '&expiredUsefulLife=' + vExpiredUsefulLife + '&ubicacion=' + vUbicacion + '&marca=' + vMarca + '&modelo=' + vModelo + '&noSerie=' + vNoSerie + '&pedimento=' + vPedimento +
				'&statusEtiquetado=' + vStatusEtiquetado + '&statusActivo=' + vStatusActivo + '&origen=' + vOrigen + '&pm=' + vPm + '&comentarios=' + vComentarios,
			success: function(data){
				$('#cajas_menu_cont').html( data );
				getInventario(0);
			},
			beforeSend :function() {
		  		$('#cajas_menu_cont').html('<div style="width=100%" align="center"><img src="loader.gif" width="66" height="66" alt="Cargando" /></div>');
	  		}
		});
	}
	
	function borrar(idMaquina){
		if (window.confirm('El registro será eliminado permanentemente. ¿Desea continuar?') == true){
			$.ajax({
				type: "POST",
				url: 'funciones.php',
				data:'cmd=delInventario&idMaquina=' + idMaquina,
				success: function(data){
					$('#cajas_menu_cont').html( data );
					getInventario(0);
				},
				beforeSend :function() {
					$('#cajas_menu_cont').html('<div style="width=100%" align="center"><img src="loader.gif" width="66" height="66" alt="Cargando" /></div>');
				}
			});
		}
	}
	
	function editarFoto(asset){
		title = 'Editar Foto';
		h = 650;
		w = 600;
		$.ajax({
			type: "POST",
		  	url: 'funciones.php',
		  	data:'cmd=editarFoto&asset=' + asset,
		  	success: function ( data ){
					if ( data == -1 ){
						alert ( 'Su sesión a expirado' );
						window.location = 'logout.php';
					}else{
						$('.cajas_menu_cont').html(data);
					}
		  	},
		  	beforeSend :function() {
			  	actualizar = false;
				$("body").append ( '<div class="cat"><div class="cajas_menu_cont" id="cajas_menu_cont"><div style="margin-top:36px; witdh=90%" align="center" >' +
									'<img src="loader.gif" width="66" height="66" alt="Cargando" /></div></div></div>');
				$('.cat').dialog({ 
					modal:true,
					closeText: 'Cerrar',
					title: title,
					height: h,
					width: w,
					close: function(){ //indicamos la función que se ejecutará al cerrarse la ventana
						$(this).remove(); //borramos la capa
							if ( actualizar == true ){
								actualizar = false;
								//location.reload ( true );
								//getInventario(0);
							}
					}
				});
		  	}
		});
	}
	
	function enviarFoto(forma){
		var fd = new FormData();
		fd.append( 'asset', forma.asset.value );
		fd.append( 'imagen', $('#imagen')[0].files[0] );
		$.ajax({
			url: 'funciones.php',
			type: 'POST',
			cache: false,
			data: fd,
			processData: false,
			contentType: false,
			success: function(data){
				$('#cajas_menu_cont').html( data );
				location.reload( true );
				//getInventario(0);
			},
			beforeSend :function() {
				$('#cajas_menu_cont').html('<div style=\"width=100%\" align=\"center\"><img src=\"loader.gif\" width=\"66\" height=\"66\" alt=\"Cargando\" /></div>');
			}
		});
	}
	
	function verFoto(nombreImagen){
		document.getElementById('fotoTReal').src = 'imgInventario/' + nombreImagen;
		$( "#dialog" ).dialog( "open" );
	}
	
	function calendario(){
		$("#capitalizedOn").datepicker( {dateFormat: 'yy-mm-dd'} );	
		$.datepicker.setDefaults($.datepicker.regional['es']);
	}
	
	$(function() {
		$( "#dialog" ).dialog({
			height:"auto",
			width:"auto",
			autoOpen: false
		});
		calendario();
	});
</script>
<body>
    <div id="dialog" title="Ver Foto">
        <img id="fotoTReal" src='' />
    </div>
	<div id="container">
    	<table class="tPrincipal" border="0" cellspacing="0" cellpadding="0">
        	<tr id="trHeader">
        	  <td colspan="3">
            	<div class="barraSup1" style="" align="right">
                	<font color="#FFFF00"><?php echo $_SESSION['usrNombre']; ?></font> <a href='logout.php' class='link1'>Cerrar sesión</a>
                </div>
                <div class="barraSup2" align="center">
                    	<img src="img/titulo.png" style="margin-bottom:17px; margin-right:100px;"/>
                        <a href="inventario.php"><img src="img/inventarioPress.png" style="margin-top:9px"/></a>
                        <a href="eventos.php"><img src="img/eventos.png" style="margin-top:9px"/></a>
                        <a href="usuarios.php"><img src="img/usuarios.png" style="margin-top:9px"/></a>
                </div>
            </td></tr>
            <tr id="trCuerpo"><!--<td width="0px"></td>-->
            	<td id="tdMainInfo" colspan="3">
                    <div class="mainInfo" align="center">
                        <p style="width:100%; text-align:left"><font size="+3">INVENTARIO</font></p><div class="barraInferior"></div>
                        <div align="right" style="float:right;">
                            <form class="fSubriInventario" action="inventario.php" enctype="multipart/form-data" method="post">
                                Importar inventario: <input type="file" name="archivoInventario" /><input id="btnSubir" type="submit" value="subir" />
                            </form>
                        </div>
                        <p style="width:100%; text-align:left">
                        <!--
                            <a href="">Borrar Seleccionado(s)</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="">Editar</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="">Agregar</a>
                         -->
                        </p>
                        <div id="divDatosInventario">
                            <?php echo getInventario('0', '', '', '', '');?>
                        </div>
                    </div>
            	</td><!--<td width="0px"></td>--></tr>
            <tr id="trPie"><td colspan="3">
                <div class="pie" style="vertical-align:bottom">
                  <p style="color:#FFF; margin-top:18px">&copy; Hersheys de Mexico 2014</p>
                </div>
            </td></tr>
        </table>
    </div>
    
    
    
    
    
    
    
    
<!--    
    <form id="MAINFORM" onsubmit="try{return gx.csv.validForm()}catch(e){return true;}" name="MAINFORM" method="post" class="Form" novalidate action="producto.aspx?INS,0">
<table id="TABLE1_MPAGE" class="Table"  data-cellpadding="0" data-cellspacing="0" style=" height: 100%; width: 100%;" ><tbody><tr><td style="vertical-align:top"><table id="TABLE2_MPAGE" class="Table"  data-cellpadding="0" data-cellspacing="0" style=" height: 100%; width: 100%;" ><tbody><tr><td class="MPBarBlue"  style="height:7px"></td></tr><tr><td><table id="TABLE3_MPAGE" class="Table"  data-cellpadding="0" data-cellspacing="0" style=" height: 100%; width: 100%;" ><tbody><tr><td class="TablePaddingLeft"  style="width:1px"></td><td><table id="TABLE4_MPAGE" class="Table"  data-cellpadding="0" data-cellspacing="0" style=" height: 100%; width: 100%;" ><tbody><tr><td style="vertical-align:top;height:70px"><table id="TABLE5_MPAGE" class="Table"  data-cellpadding="0" data-cellspacing="0" style=" height: 100%; width: 100%;" ><tbody><tr><td style="vertical-align:middle"></td><td data-align="right"  style="text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;vertical-align:bottom"><table id="TABLE6_MPAGE" class="Table"  data-cellpadding="1" data-cellspacing="2" style="margin-left:auto; width: 100%;" ><tbody><tr><td data-align="right"  style="text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;height:50px;width:582px"><table id="TABLE7_MPAGE" class="Table"  data-cellpadding="1" data-cellspacing="2" style=" height: 100%; width: 100%;" ><tbody><tr><td data-align="right"  style="text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;height:20px"><span class="ReadonlyUserNickName"  style=";height: 1row;"  id="span_vUSERNAME_MPAGE">An&#243;nimo</span></td></tr><tr><td data-align="right"  style="text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;vertical-align:top;height:22px"><span class="ReadonlyUserPosition"  style=";height: 1row;"  id="span_vROLEDESCRIPTION_MPAGE">Administrador</span></td></tr></tbody></table></td><td data-align="right"  style="text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;width:1px"><input type="image" src="Resources/ActionLogOut.png" name="IMAGE2_MPAGE" onclick="if( gx.evt.jsEvent(this)) {gx.evt.execCliEvt('',true,'e180b1_client',this);return false;} else return false;" id="IMAGE2_MPAGE" alt=" " class="Image"   onfocus="gx.evt.onfocus(this, 32,'',true,'',0)"/></td><td data-align="right"  style="text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;width:1px"><img src="Resources/help.png" id="IMAGE3_MPAGE" alt=" " class="Image" /></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="height:70px"><div class="gx_usercontrol" id="MPDROPDOWNTABSMENU1Container"></div></td></tr><tr><td><table id="TABLE8_MPAGE" class="Table"  data-cellpadding="0" data-cellspacing="0" style=" height: 100%; width: 100%;" ><tbody><tr><td class="MasterLeftZone"  style="vertical-align:top;width:260px"><table id="TABLE9_MPAGE" class="Table"  data-cellpadding="0" data-cellspacing="0" style=" width: 240px;" ><tbody><tr><td data-align="center"  style="text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top;height:60px"><input type="button" name="BUTTON1_MPAGE" value="Acci&#243;n" title="Acci&#243;n" class="btn_Master btn btn-large btn-block btn-primary"  onclick="if( gx.evt.jsEvent(this)) {gx.evt.execEvt('EENTER_MPAGE.',this);} else return false;"   onfocus="gx.evt.onfocus(this, 46,'',true,'',0)"/></td></tr><tr><td style="vertical-align:top;height:151px;width:260px"><div class="gx_usercontrol" id="MPDVELOP_BOOTSTRAP_PANEL1Container"></div><div class="gx_usercontrol_child" id="MPDVELOP_BOOTSTRAP_PANEL1ContainerBody" style="display:none;"><table id="TABLE18_MPAGE" class="Table100x100"  data-cellpadding="0" data-cellspacing="0"><tbody><tr><td><table id="TABLE10_MPAGE" class="TableRowSideBar"  data-cellpadding="4" data-cellspacing="4"><tbody><tr><td background=""  style="vertical-align:middle"><img src="Resources/menu_icon1.png" id="IMAGE1_MPAGE" alt=" " class="Image" />&nbsp;</td><td><span class="MenuItem"  id="MENUITEM1_MPAGE" ><a href="javascript:gx.evt.execCliEvt('',true,'e170b1_client',this);">Acceso directo 1</a></span></td></tr></tbody></table></td></tr><tr><td><table id="TABLE12_MPAGE" class="TableRowSideBar"  data-cellpadding="4" data-cellspacing="4"><tbody><tr><td><img src="Resources/menu_icon2.png" id="IMAGE8_MPAGE" alt=" " class="Image" />&nbsp;</td><td><span class="MenuItem"  id="MENUITEM2_MPAGE" ><a href="javascript:gx.evt.execCliEvt('',true,'e160b1_client',this);">Acceso directo 2</a></span></td></tr></tbody></table></td></tr><tr><td><table id="TABLE13_MPAGE" class="TableRowSideBar"  data-cellpadding="4" data-cellspacing="4"><tbody><tr><td><img src="Resources/menu_icon3.png" id="IMAGE9_MPAGE" alt=" " class="Image" />&nbsp;</td><td><span class="MenuItem"  id="MENUITEM3_MPAGE" ><a href="javascript:gx.evt.execCliEvt('',true,'e150b1_client',this);">Acceso directo 3</a></span></td></tr></tbody></table></td></tr><tr><td><table id="TABLE14_MPAGE" class="TableRowSideBar"  data-cellpadding="4" data-cellspacing="4"><tbody><tr><td><img src="Resources/menu_icon4.png" id="IMAGE4_MPAGE" alt=" " class="Image" />&nbsp;</td><td><span class="MenuItem"  id="MENUITEM4_MPAGE" ><a href="javascript:gx.evt.execCliEvt('',true,'e140b1_client',this);">Acceso directo 4</a></span></td></tr></tbody></table></td></tr><tr><td><table id="TABLE15_MPAGE" class="TableRowSideBar"  data-cellpadding="4" data-cellspacing="4"><tbody><tr><td><img src="Resources/menu_icon5.png" id="IMAGE5_MPAGE" alt=" " class="Image" />&nbsp;</td><td><span class="MenuItem"  id="MENUITEM5_MPAGE" ><a href="javascript:gx.evt.execCliEvt('',true,'e130b1_client',this);">Acceso directo 5</a></span></td></tr></tbody></table></td></tr></tbody></table></div></td></tr><tr><td><div class="gx_usercontrol" id="MPDVELOP_BOOTSTRAP_TOOLTIP1Container"></div></td></tr></tbody></table></td><td class="MasterRightZone"  style="vertical-align:top"><table id="TABLEMAIN" class="TableMain"  data-cellpadding="1" data-cellspacing="2"><tbody><tr><td><span class="TextBlockTitleWWP"  id="PRODUCTOTITLE" >Producto</span></td></tr><tr><td><table id="TABLECONTENT" class="TableContent"  data-cellpadding="0" data-cellspacing="0"><tbody><tr><td><div><span class="ErrorViewer gx_ev ErrorViewerBullet"  id="gxErrorViewer"></span></div></td></tr><tr><td><div class="gx_usercontrol" id="DVPANEL_TABLEATTRIBUTESContainer"></div><div class="gx_usercontrol_child" id="DVPANEL_TABLEATTRIBUTESContainerBody" style="display:none;"><table id="TABLEATTRIBUTES" class="TableData"  data-cellpadding="1" data-cellspacing="1"><tbody><tr><td class="DataDescriptionCell" ><span class="DataDescription"  id="TEXTBLOCKPRODUCTOID" >Id</span></td><td class="DataContentCell" ><span class="ReadonlyBootstrapAttribute"  style=";height: 1row;"  id="span_PRODUCTOID">   0</span></td></tr><tr><td class="DataDescriptionCell" ><span class="DataDescription"  id="TEXTBLOCKPRODUCTONOMBRE" >Nombre</span></td><td class="DataContentCell" ><input type="text" id="PRODUCTONOMBRE" name="PRODUCTONOMBRE" value="" size="40" spellcheck="true" maxlength="40" class="BootstrapAttribute"  style=";height: 1row;text-align:left"   onfocus="gx.evt.onfocus(this, 26,'',false,'',0)" onchange="gx.evt.onchange(this)"  onblur=";gx.evt.onblur(26);"/></td></tr><tr><td class="DataDescriptionCell" ><span class="DataDescription"  id="TEXTBLOCKPRODUCTOPRECIO" >Precio</span></td><td class="DataContentCell" ><input type="text" id="PRODUCTOPRECIO" name="PRODUCTOPRECIO" value="0.00" size="18" spellcheck="false" maxlength="18" class="BootstrapAttribute"  style=";height: 1row;text-align:right"   onfocus="gx.evt.onfocus(this, 31,'',false,'',0)" onchange="gx.evt.onchange(this)"  onblur="gx.num.valid_decimal( this, ',','.','2');;gx.evt.onblur(31);"/></td></tr></tbody></table></div></td></tr></tbody></table></td></tr><tr><td><table id="TABLEACTIONS" class="TableActions"  data-cellpadding="0" data-cellspacing="0"><tbody><tr><td class="TableActionsCell" ><input type="button" name="BTN_TRN_ENTER" value="Confirmar" title="Confirmar" class="btn btn-primary"  onclick="if( gx.evt.jsEvent(this)) {gx.evt.execEvt('EENTER.',this);} else return false;"   onfocus="gx.evt.onfocus(this, 37,'',false,'',0)"/></td><td class="TableActionsCell" ><input type="button" name="BTN_TRN_CANCEL" value="Cancelar" title="Cancelar" class="btn btn-default"  onclick="if( gx.evt.jsEvent(this)) {gx.fn.closeWindow();return false;} else return false;"   onfocus="gx.evt.onfocus(this, 39,'',false,'',0)"/></td><td class="TableActionsCell" ><input type="button" name="BTN_TRN_DELETE" value="Eliminar" title="Eliminar" class="btn btn-default"  style=";display:none;"  disabled="disabled" onclick="if( gx.evt.jsEvent(this)) {gx.evt.execEvt('EDELETE.',this);} else return false;"   onfocus="gx.evt.onfocus(this, 41,'',false,'',0)"/></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td><td style="width:15px"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>
<div><input type="hidden" name="GXState" value='{"_EventName":"","_EventGridId":"","_EventRowId":"","vUSERNAME_MPAGE":"Anónimo","vROLEDESCRIPTION_MPAGE":"Administrador","PRODUCTOID":"0","vTABSMENUDATA_MPAGE":[{"MenuId":1,"MenuTitle":"Inicio","MenuDescription":"Inicio","MenuURL":"home.aspx","Sections":[{"SectionId":0,"SectionTitle":"","SectionDescription":"","SectionURL":""}]},{"MenuId":2,"MenuTitle":"Opción de Menú 2","MenuDescription":"Opción de Menú 2","MenuURL":"","Sections":[{"SectionId":0,"SectionTitle":"","SectionDescription":"","SectionURL":""}]},{"MenuId":3,"MenuTitle":"Opción de Menú 3","MenuDescription":"Opción de Menú 3","MenuURL":"","Sections":[{"SectionId":0,"SectionTitle":"","SectionDescription":"","SectionURL":""}]},{"MenuId":4,"MenuTitle":"Opción de Menú 4","MenuDescription":"Opción de Menú 4","MenuURL":"","Sections":[{"SectionId":0,"SectionTitle":"","SectionDescription":"","SectionURL":""}]},{"MenuId":5,"MenuTitle":"Menú de Desarrollo","MenuDescription":"Menú de Desarrollo","MenuURL":"","Sections":[{"SectionId":6,"SectionTitle":" Factura","SectionDescription":" Factura","SectionURL":"wwfactura.aspx"},{"SectionId":7,"SectionTitle":" Producto","SectionDescription":" Producto","SectionURL":"wwproducto.aspx"}]}],"DROPDOWNTABSMENU1_MPAGE_Menustyle":"bluetabs","DVELOP_BOOTSTRAP_PANEL1_MPAGE_Width":"100","DVELOP_BOOTSTRAP_PANEL1_MPAGE_Cls":"BootstrapPanel-Gray","DVELOP_BOOTSTRAP_PANEL1_MPAGE_Title":"Título del menú","DVELOP_BOOTSTRAP_PANEL1_MPAGE_Collapsible":"false","DVELOP_BOOTSTRAP_PANEL1_MPAGE_Autoheight":"true","DVELOP_BOOTSTRAP_PANEL1_MPAGE_Showcollapseicon":"false","DVELOP_BOOTSTRAP_PANEL1_MPAGE_Iconposition":"right","DVELOP_BOOTSTRAP_PANEL1_MPAGE_Autoscroll":"false","Z1ProductoId":"0","Z2ProductoNombre":"","Z3ProductoPrecio":"0.00","IsConfirmed":"0","IsModified":"0","Mode":"INS","vPRODUCTOID":"0","vMODE":"INS","DVPANEL_TABLEATTRIBUTES_Width":"100","DVPANEL_TABLEATTRIBUTES_Cls":"GXUI-DVelop-Panel","DVPANEL_TABLEATTRIBUTES_Title":"Información General","DVPANEL_TABLEATTRIBUTES_Collapsible":"false","DVPANEL_TABLEATTRIBUTES_Collapsed":"false","DVPANEL_TABLEATTRIBUTES_Autowidth":"false","DVPANEL_TABLEATTRIBUTES_Autoheight":"true","DVPANEL_TABLEATTRIBUTES_Showcollapseicon":"false","DVPANEL_TABLEATTRIBUTES_Iconposition":"left","DVPANEL_TABLEATTRIBUTES_Autoscroll":"false","GX_FocusControl":"PRODUCTONOMBRE","GX_AJAX_KEY":"685657B5D287515EF26693D7634D004B","AJAX_SECURITY_TOKEN":"0AF33A09CCDFAA7672BDB8D0232E8F114F8C667478BE0D08E0743A7F1F3D53B7","GX_CMP_OBJS":{},"sCallerURL":"http://disc.com.mx/wssaturno/wwproducto.aspx","GX_CLI_NAV":"true","GX_RES_PROVIDER":"GXResourceProvider.aspx","GX_THEME":"WorkWithPlusBootstrapTheme"}'/></div>
</form>
    
    -->
    
    
    
    
</body>
</html>