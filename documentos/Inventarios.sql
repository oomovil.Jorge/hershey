CREATE DATABASE  IF NOT EXISTS `inventarios` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `inventarios`;
-- MySQL dump 10.13  Distrib 5.5.16, for osx10.5 (i386)
--
-- Host: 127.0.0.1    Database: inventarios
-- ------------------------------------------------------
-- Server version	5.5.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eventos`
--

DROP TABLE IF EXISTS `eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `fechaInicial` date DEFAULT NULL,
  `fechaFinal` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eventosInventario`
--

DROP TABLE IF EXISTS `eventosInventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventosInventario` (
  `eventosInventario_id` int(11) NOT NULL AUTO_INCREMENT,
  `eventos_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `usuarios_login` varchar(20) NOT NULL,
  `origenRegistro` varchar(10) NOT NULL,
  `asset` varchar(15) DEFAULT NULL,
  `subnumber` varchar(15) DEFAULT NULL,
  `capitalizedOn` date DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `acquisVal` double DEFAULT NULL,
  `accumDep` double DEFAULT NULL,
  `bookVal` double DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `plant` varchar(20) DEFAULT NULL,
  `respCostCenter` varchar(20) DEFAULT NULL,
  `order` varchar(20) DEFAULT NULL,
  `expiredUsefulLife` int(11) DEFAULT NULL,
  `ubicacion` varchar(200) DEFAULT NULL,
  `marca` varchar(100) DEFAULT NULL,
  `modelo` varchar(50) DEFAULT NULL,
  `noSerie` varchar(100) DEFAULT NULL,
  `pedimento` varchar(50) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `statusEtiquetado` varchar(50) DEFAULT NULL,
  `statusActivo` varchar(20) DEFAULT NULL,
  `origen` varchar(20) DEFAULT NULL,
  `comentarios` text,
  PRIMARY KEY (`eventosInventario_id`,`eventos_id`,`id`,`usuarios_login`,`origenRegistro`),
  KEY `fk_eventosInventario_eventos1` (`eventos_id`),
  KEY `fk_eventosInventario_usuarios1` (`usuarios_login`),
  CONSTRAINT `fk_eventosInventario_eventos1` FOREIGN KEY (`eventos_id`) REFERENCES `eventos` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_eventosInventario_usuarios1` FOREIGN KEY (`usuarios_login`) REFERENCES `usuarios` (`login`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inventario`
--

DROP TABLE IF EXISTS `inventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset` varchar(15) DEFAULT NULL,
  `subnumber` varchar(15) DEFAULT NULL,
  `capitalizedOn` date DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `acquisVal` double DEFAULT NULL,
  `accumDep` double DEFAULT NULL,
  `bookVal` double DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `plant` varchar(20) DEFAULT NULL,
  `respCostCenter` varchar(20) DEFAULT NULL,
  `order` varchar(20) DEFAULT NULL,
  `expiredUsefulLife` int(11) DEFAULT NULL,
  `ubicacion` varchar(200) DEFAULT NULL,
  `marca` varchar(100) DEFAULT NULL,
  `modelo` varchar(50) DEFAULT NULL,
  `noSerie` varchar(100) DEFAULT NULL,
  `pedimento` varchar(50) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `statusEtiquetado` varchar(50) DEFAULT NULL,
  `statusActivo` varchar(20) DEFAULT NULL,
  `origen` varchar(20) DEFAULT NULL,
  `comentarios` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `login` varchar(20) NOT NULL,
  `pass` varchar(50) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `usrTablet` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eventosParticipantes`
--

DROP TABLE IF EXISTS `eventosParticipantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventosParticipantes` (
  `eventos_id` int(11) NOT NULL,
  `usuarios_login` varchar(20) NOT NULL,
  PRIMARY KEY (`eventos_id`,`usuarios_login`),
  KEY `fk_eventosParticipantes_usuarios1` (`usuarios_login`),
  CONSTRAINT `fk_eventosParticipantes_eventos` FOREIGN KEY (`eventos_id`) REFERENCES `eventos` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_eventosParticipantes_usuarios1` FOREIGN KEY (`usuarios_login`) REFERENCES `usuarios` (`login`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-10 12:00:05
