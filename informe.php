<?php
	include ( 'funciones.php' );
	date_default_timezone_set('America/Mexico_City');
	require_once("Classes/PHPExcel.php"); 
	require_once("Classes/PHPExcel/IOFactory.php");
		if(!isset($_POST['idEvento']) && !isset($_POST['tipoReporte']))
			return;
	$filename = "";
	$tipoReporte = $_POST['tipoReporte'];
	$idEvento = $_POST['idEvento'];
	$db = getConexion($cnx);
		if($db->conecta()){
			$query = "select *, date_format( fechaInicial, '%d-%m-%Y' ) fechaInicial2, date_format( fechaFinal, '%d-%m-%Y' ) fechaFinal2
						from eventos where id = $idEvento";
			$link = $db->db_query($query) or die("Error en la consulta: $query");
			$datos = $db->db_fetch_object($link);
				if(!isset($datos->id))
					return;
			$queryReporte = "";
			$nombreEvento = $datos->nombre;
			$fechaInicial = $datos->fechaInicial;
			$fechaFinal = $datos->fechaFinal;
				if($tipoReporte == "rep1"){
					$filename = "Inventario SAP faltante.xls";
					$queryReporte = "select *, date_format( capitalizedOn, '%d-%m-%Y' ) capitalizedOn2 
									from eventosInventario 
									where eventos_id = $idEvento and subnumber = '0' and 
											asset not in ( select asset from eventosInventario where eventos_id = $idEvento and origenRegistro = 'Tablet' )
									order by asset asc, origenRegistro desc";
				}else
					if($tipoReporte == "rep2"){
						$filename = "Inventario tablet sin registro SAP.xls";
						$queryReporte = "select *, date_format( capitalizedOn, '%d-%m-%Y' ) capitalizedOn2
										from eventosInventario 
										where eventos_id = $idEvento and origenRegistro = 'Tablet' and
											asset not in ( select asset from eventosInventario where eventos_id = $idEvento and origenRegistro = 'Inventario' and subnumber = '0' )
										order by asset asc, origenRegistro desc";
					}else
						if($tipoReporte == "rep3"){
							$filename = "Todo el evento.xls";
							$queryReporte = "select *, date_format( capitalizedOn, '%d-%m-%Y' ) capitalizedOn2
											from eventosInventario 
											where eventos_id = $idEvento
											order by asset asc, origenRegistro desc";
						}else 
							if($tipoReporte == "rep4"){
								$filename = "Inventario tablet.xls";
								$queryReporte = "select *, date_format( capitalizedOn, '%d-%m-%Y' ) capitalizedOn2
												from eventosInventario 
												where eventos_id = $idEvento and origenRegistro = 'Tablet'
												order by asset asc, origenRegistro desc";
							}else return;
			//$filename = 'informe.xls';
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', $nombreEvento )
				->setCellValue('B1', $fechaInicial )
				->setCellValue('C1', $fechaFinal )
				->setCellValue('A2', 'Usuario' )
				->setCellValue('B2', 'Tipo' )
				->setCellValue('C2', 'Asset' )
				->setCellValue('D2', 'Subnumber' )
				->setCellValue('E2', 'CapitalizedOn' )
				->setCellValue('F2', 'Description' )
				->setCellValue('G2', 'Acquis Val.' )
				->setCellValue('H2', 'Accum Dep.' )
				->setCellValue('I2', 'Book Val.' )
				->setCellValue('J2', 'Currency' )
				->setCellValue('K2', 'Plant' )
				->setCellValue('L2', 'PM' )
				->setCellValue('M2', 'Resp Cost Center' )
				->setCellValue('N2', 'Order' )
				->setCellValue('O2', 'Ubicacion' )
				->setCellValue('P2', 'Marca' )
				->setCellValue('Q2', 'Modelo' )
				->setCellValue('R2', 'No Serie' )
				->setCellValue('S2', 'Comentario' )
				->setCellValue('T2', 'Etiquetado' )
				->setCellValue('U2', 'Pedimento' )
				;
			$objPHPExcel->getActiveSheet()->setTitle('Info');
			$excelRen = 3;
			$query = $queryReporte;
			$link = $db->db_query($query) or die("Error en la consulta: $query");
				while($row = $db->db_fetch_object($link)){
					$objPHPExcel->getActiveSheet()->setCellValue("A" . $excelRen, $row->usuarios_login);
					$objPHPExcel->getActiveSheet()->setCellValue("B" . $excelRen, $row->origenRegistro);
					$objPHPExcel->getActiveSheet()->setCellValue("C" . $excelRen, $row->asset);
					$objPHPExcel->getActiveSheet()->setCellValue("D" . $excelRen, $row->subnumber);
					$objPHPExcel->getActiveSheet()->setCellValue("E" . $excelRen, $row->capitalizedOn);
					$objPHPExcel->getActiveSheet()->setCellValue("F" . $excelRen, $row->description);
					$objPHPExcel->getActiveSheet()->setCellValue("G" . $excelRen, $row->acquisVal);
					$objPHPExcel->getActiveSheet()->setCellValue("H" . $excelRen, $row->accumDep);
					$objPHPExcel->getActiveSheet()->setCellValue("I" . $excelRen, $row->bookVal);
					$objPHPExcel->getActiveSheet()->setCellValue("J" . $excelRen, $row->currency);
					$objPHPExcel->getActiveSheet()->setCellValue("K" . $excelRen, $row->plant);
					$objPHPExcel->getActiveSheet()->setCellValue("L" . $excelRen, $row->pm);
					$objPHPExcel->getActiveSheet()->setCellValue("M" . $excelRen, $row->respCostCenter);
					$objPHPExcel->getActiveSheet()->setCellValue("N" . $excelRen, $row->order);
					$objPHPExcel->getActiveSheet()->setCellValue("O" . $excelRen, $row->ubicacion);
					$objPHPExcel->getActiveSheet()->setCellValue("P" . $excelRen, $row->marca);
					$objPHPExcel->getActiveSheet()->setCellValue("Q" . $excelRen, $row->modelo);
					$objPHPExcel->getActiveSheet()->setCellValue("R" . $excelRen, $row->noSerie);
					$objPHPExcel->getActiveSheet()->setCellValue("S" . $excelRen, $row->comentarios);
					$objPHPExcel->getActiveSheet()->setCellValue("T" . $excelRen, $row->statusEtiquetado);
					$objPHPExcel->getActiveSheet()->setCellValue("U" . $excelRen, $row->pedimento);
					$excelRen++;
				}
			$objPHPExcel->setActiveSheetIndex(0);
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="' . $filename . '"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
		}
?>