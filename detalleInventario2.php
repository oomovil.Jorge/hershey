﻿<?php
	//session_start();
	include ( 'funciones.php' );
		if(!isset($_SESSION['usrLogeado']) || !isset($_GET['idEvento']))
			header( 'location: index.php' );
	$idEvento = $_GET['idEvento'];
	$nombreInventario = getNombreEvento($_GET['idEvento']);
		if($nombreInventario == "")
			header( 'location: index.php' );
	//$usuarioLogeado = '<font color="#FFFF00">' . $_SESSION['usrNombre'] . "</font> <a href='logout.php' class='link1'>Cerrar sesión</a>";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Hersheys Inventarios</title>
</head>
<link href="main.css" rel="stylesheet" type="text/css" />
<link href="jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="jquery-ui.js"></script>
<script>
	function mostrarFiltro(id){
		if ( $( "#" + id ).is ( ':visible' ) ) {
			$( "#" + id ).hide ( 'slow' );
		}else{
			$( "#" + id ).show ( 'slow' )
		}
	}

	function getEventoInventario(idEvento, pagina){
		vbuscar = document.getElementById('buscar').value;
		buscarCol = document.getElementById('sColBuscar').value;
		ordenCol = document.getElementById('sColOrdenar').value;
		ordenDir = document.getElementById('sOrdeDireccion').value;
		$.ajax({
			type: "POST",
			url: 'funciones.php',
			data:'cmd=getEventoInventario&idEvento=' + idEvento + '&buscar=' + vbuscar + '&buscarCol=' + buscarCol + '&ordenCol=' + ordenCol + '&ordenDir=' + ordenDir + '&pagina=' + pagina,
			success: function(data){
				$('#divDatosInventario').html( data );	
			},
			beforeSend :function() {
		  		$('#divDatosInventario').html('<br><br><div style="width=100%" align="center"><img src="loader.gif" width="562" height="126" alt="Cargando" /></div>');
	  		}
		});
	}
	
	function addInventario(idEvento){
		title = 'Agregar Inventario al Evento';
		h = 920;
		w = 600;
		$.ajax({
			type: "POST",
		  	url: 'funciones.php',
		  	data:'cmd=addEventoInventario&idEvento=' + idEvento,
		  	success: function ( data ){
					if ( data == -1 ){
						alert ( 'Su sesión a expirado' );
						window.location = 'logout.php';
					}else{
						$('.cajas_menu_cont').html(data);
					}
		  	},
		  	beforeSend :function() {
			  	actualizar = false;
				$("body").append ( '<div class="cat"><div class="cajas_menu_cont" id="cajas_menu_cont"><div style="margin-top:36px; witdh=90%" align="center" >' +
									'<img src="loader.gif" width="562" height="126" alt="Cargando" /></div></div></div>');
				$('.cat').dialog({ modal:true,
					closeText: 'Cerrar',
					title: title,
					height: h,
					width: w,
					close: function(){ //indicamos la función que se ejecutará al cerrarse la ventana
						$(this).remove(); //borramos la capa
						getEventoInventario(idEvento, 0);
					}
				});
		  	}
		});
	}
	
	function validarEventoInventario(obj){
		if (obj.asset.value.length == 0) {
				document.getElementById('errordial').innerHTML='El campo Asset no puede estar vacio.';
				$("#errordial").slideDown('slow')
				return false;
		}else{
			return true;
		}
	}
	
	function agregaEventoInventario(obj){
		idEvento = obj.idDelEvento.value;
		vAsset = obj.asset.value;
		vSubnumber = obj.subnumber.value;
		vCapitalizedOnYear = obj.capitalizedOnYear.value;
		vCapitalizedOnMonth = obj.capitalizedOnMonth.value;
		vCapitalizedOnDay = obj.capitalizedOnDay.value;
		vDescription = obj.description.value;
		vAcquisVal = obj.acquisVal.value;
		vAccumDep = obj.accumDep.value;
		vBookVal = obj.bookVal.value;
		vCurrency = obj.currency.value;
		vPlant = obj.plant.value;
		vRespCostCenter = obj.respCostCenter.value;
		vOrder = obj.order.value;
		vExpiredUsefulLife = obj.expiredUsefulLife.value;
		vUbicacion = obj.ubicacion.value;
		vMarca = obj.marca.value;
		vModelo = obj.modelo.value;
		vNoSerie = obj.noSerie.value;
		vPedimento = obj.pedimento.value;
		vTipo = obj.tipo.value;
		vStatusEtiquetado = obj.statusEtiquetado.value;
		vStatusActivo = obj.statusActivo.value;
		vOrigen = obj.origen.value;
		vComentarios = obj.comentarios.value;
		$.ajax({
			type: "POST",
			url: 'funciones.php',
			data:'cmd=addEventoInventario&idDelEvento=' + idEvento + '&asset=' + vAsset + '&subnumber=' + vSubnumber + '&capitalizedOnY=' + vCapitalizedOnYear + '&capitalizedOnM=' + vCapitalizedOnMonth + '&capitalizedOnD=' + vCapitalizedOnDay +
				'&description=' + vDescription + '&acquisVal=' + vAcquisVal + '&accumDep=' + vAccumDep + '&bookVal=' + vBookVal + '&currency=' + vCurrency + '&plant=' + vPlant + '&respCostCenter=' + vRespCostCenter +
				'&order=' + vOrder + '&expiredUsefulLife=' + vExpiredUsefulLife + '&ubicacion=' + vUbicacion + '&marca=' + vMarca + '&modelo=' + vModelo + '&noSerie=' + vNoSerie + '&pedimento=' + vPedimento +
				'&statusEtiquetado=' + vStatusEtiquetado + '&statusActivo=' + vStatusActivo + '&origen=' + vOrigen + '&comentarios=' + vComentarios + '&tipo=' + vTipo,
			success: function(data){
				$('#cajas_menu_cont').html( data );	
			},
			beforeSend :function() {
		  		$('#cajas_menu_cont').html('<div style="width=100%" align="center"><img src="loader.gif" width="562" height="126" alt="Cargando" /></div>');
	  		}
		});
	}
	
	function editar(idEvento,idMaquina){
		title = 'Editar Maquina del Evento';
		h = 920;
		w = 600;
		$.ajax({
			type: "POST",
		  	url: 'funciones.php',
		  	data:'cmd=editEventoInventario&id=' + idMaquina,
		  	success: function ( data ){
					if ( data == -1 ){
						alert ( 'Su sesión a expirado' );
						window.location = 'logout.php';
					}else{
						$('.cajas_menu_cont').html(data);
					}
		  	},
		  	beforeSend :function() {
			  	actualizar = false;
				$("body").append ( '<div class="cat"><div class="cajas_menu_cont" id="cajas_menu_cont"><div style="margin-top:36px; witdh=90%" align="center" >' +
									'<img src="loader.gif" width="562" height="126" alt="Cargando" /></div></div></div>');
				$('.cat').dialog({ modal:true,
					closeText: 'Cerrar',
					title: title,
					height: h,
					width: w,
					close: function(){ //indicamos la función que se ejecutará al cerrarse la ventana
						$(this).remove(); //borramos la capa
						getEventoInventario(idEvento,0);
					}
				});
		  	}
		});
	
	}
	
	function editaEventoInventario(obj){
		idEvento = obj.idEvento.value;
		id = obj.idMaquina.value;
		vAsset = obj.asset.value;
		vSubnumber = obj.subnumber.value;
		vCapitalizedOnYear = obj.capitalizedOnYear.value;
		vCapitalizedOnMonth = obj.capitalizedOnMonth.value;
		vCapitalizedOnDay = obj.capitalizedOnDay.value;
		vDescription = obj.description.value;
		vAcquisVal = obj.acquisVal.value;
		vAccumDep = obj.accumDep.value;
		vBookVal = obj.bookVal.value;
		vCurrency = obj.currency.value;
		vPlant = obj.plant.value;
		vRespCostCenter = obj.respCostCenter.value;
		vOrder = obj.order.value;
		vExpiredUsefulLife = obj.expiredUsefulLife.value;
		vUbicacion = obj.ubicacion.value;
		vMarca = obj.marca.value;
		vModelo = obj.modelo.value;
		vNoSerie = obj.noSerie.value;
		vPedimento = obj.pedimento.value;
		vTipo = obj.tipo.value;
		vStatusEtiquetado = obj.statusEtiquetado.value;
		vStatusActivo = obj.statusActivo.value;
		vOrigen = obj.origen.value;
		vComentarios = obj.comentarios.value;
		$.ajax({
			type: "POST",
			url: 'funciones.php',
			data:'cmd=editEventoInventario&idMaquina=' + id + '&asset=' + vAsset + '&subnumber=' + vSubnumber + '&capitalizedOnY=' + vCapitalizedOnYear + '&capitalizedOnM=' + vCapitalizedOnMonth + '&capitalizedOnD=' + vCapitalizedOnDay +
				'&description=' + vDescription + '&acquisVal=' + vAcquisVal + '&accumDep=' + vAccumDep + '&bookVal=' + vBookVal + '&currency=' + vCurrency + '&plant=' + vPlant + '&respCostCenter=' + vRespCostCenter +
				'&order=' + vOrder + '&expiredUsefulLife=' + vExpiredUsefulLife + '&ubicacion=' + vUbicacion + '&marca=' + vMarca + '&modelo=' + vModelo + '&noSerie=' + vNoSerie + '&pedimento=' + vPedimento +
				'&statusEtiquetado=' + vStatusEtiquetado + '&statusActivo=' + vStatusActivo + '&origen=' + vOrigen + '&comentarios=' + vComentarios + '&tipo=' + vTipo,
			success: function(data){
				$('#cajas_menu_cont').html( data );
				getEventoInventario(idEvento, 0);
			},
			beforeSend :function() {
		  		$('#cajas_menu_cont').html('<div style="width=100%" align="center"><img src="loader.gif" width="562" height="126" alt="Cargando" /></div>');
	  		}
		});
	}
	
	function borrar(idEvento, idMaquina){
		if (window.confirm('El registro será eliminado permanentemente. ¿Desea continuar?') == true){
			$.ajax({
				type: "POST",
				url: 'funciones.php',
				data:'cmd=delEventoInventario&idMaquina=' + idMaquina,
				success: function(data){
					$('#cajas_menu_cont').html( data );
					getEventoInventario(idEvento, 0);
				},
				beforeSend :function() {
					$('#cajas_menu_cont').html('<div style="width=100%" align="center"><img src="loader.gif" width="562" height="126" alt="Cargando" /></div>');
				}
			});
		}
	}
	
	function verFoto(rutaImagen){
		document.getElementById('fotoTReal').src = rutaImagen;
		$( "#dialog" ).dialog( "open" );
	}
	
	$(function(){
		$( "#dialog" ).dialog({
			height:"auto",
			width:"auto",
			autoOpen: false
		});
	});
</script>
<body>
    <div id="dialog" title="Ver Foto">
        <img id="fotoTReal" src='' />
    </div>
	<div id="container">
    	<table class="tPrincipal" border="0" cellspacing="0" cellpadding="0">
        	<tr id="trHeader">
        	  <td colspan="3">
            	<div class="barraSup1" style="" align="right">
                	<font color="#FFFF00"><?php echo $_SESSION['usrNombre']; ?></font> <a href='logout.php' class='link1'>Cerrar sesión</a>
                </div>
                <div class="barraSup2" align="center">
                    	<img src="img/titulo.png" style="margin-bottom:17px; margin-right:100px;"/>
                        <a href="inventario.php"><img src="img/inventario.png" style="margin-top:9px"/></a>
                        <a href="eventos.php"><img src="img/eventosPress.png" style="margin-top:9px"/></a>
                        <a href="usuarios.php"><img src="img/usuarios.png" style="margin-top:9px"/></a>
                </div>
            </td></tr>
            <tr id="trCuerpo"><!--<td width="0px"></td>-->
            	<td id="tdMainInfo" colspan="3">
                    <div class="mainInfo" align="center">
                        <p style="width:100%; text-align:left"><font size="+3">INVENTARIO <?php echo $nombreInventario; ?></font></p><div class="barraInferior"></div>
                        <div id="divDatosInventario">
                            <?php echo getEventoInventario($idEvento, '0', '', '', '', '');?>
                        </div>
                    </div>
            	</td><!--<td width="0px"></td>--></tr>
            <tr id="trPie"><td colspan="3">
                <div class="pie" style="vertical-align:bottom">
                  <p style="color:#FFF; margin-top:18px">&copy; Hersheys de Mexico 2014</p>
                </div>
            </td></tr>
        </table>
    </div>
</body>
</html>