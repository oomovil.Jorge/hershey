﻿<?php
	//session_start();
	include ( 'funciones.php' );
		if(!isset($_SESSION['usrLogeado']))
			header( 'location: index.php' );
	//$usuarioLogeado = '<font color="#FFFF00">' . $_SESSION['usrNombre'] . "</font> <a href='logout.php' class='link1'>Cerrar sesión</a>";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Hersheys Inventarios</title>
</head>
<link href="main.css" rel="stylesheet" type="text/css" />
<link href="jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="jquery-ui.js"></script>
<script>
	function mostrarFiltro(id){
		if ( $( "#" + id ).is ( ':visible' ) ) {
			$( "#" + id ).hide ( 'slow' );
		}else{
			$( "#" + id ).show ( 'slow' )
		}
	}

	function getUsuarios(pagina){
		vbuscar = /*document.getElementById('buscar').value*/ '';
		buscarCol = /*document.getElementById('sColBuscar').value*/ '';
		ordenCol = /*document.getElementById('sColOrdenar').value*/ '';
		ordenDir = /*document.getElementById('sOrdeDireccion').value*/ '';
		$.ajax({
			type: "POST",
			url: 'funciones.php',
			data:'cmd=getUsuarios&buscar=' + vbuscar + '&buscarCol=' + buscarCol + '&ordenCol=' + ordenCol + '&ordenDir=' + ordenDir + '&pagina=' + pagina,
			success: function(data){
				$('#divDatosUsuarios').html( data );	
			},
			beforeSend :function() {
		  		$('#divDatosUsuarios').html('<div style="width=100%" align="center"><img src="loader.gif" width="562" height="126" alt="Cargando" /></div>');
	  		}
		});
	}
	
	function addUsuario(){
		title = 'Agregar Usuario';
		h = 520;
		w = 600;
		$.ajax({
			type: "POST",
		  	url: 'funciones.php',
		  	data:'cmd=addUsuario',
		  	success: function ( data ){
					if ( data == -1 ){
						alert ( 'Su sesión a expirado' );
						window.location = 'logout.php';
					}else{
						$('.cajas_menu_cont').html(data);
					}
		  	},
		  	beforeSend :function() {
			  	actualizar = false;
				$("body").append ( '<div class="cat"><div class="cajas_menu_cont" id="cajas_menu_cont"><div style="margin-top:36px; witdh=90%" align="center" >' +
									'<img src="loader.gif" width="562" height="126" alt="Cargando" /></div></div></div>');
				$('.cat').dialog({ modal:true,
					closeText: 'Cerrar',
					title: title,
					height: h,
					width: w,
					close: function(){ //indicamos la función que se ejecutará al cerrarse la ventana
						$(this).remove(); //borramos la capa
						getUsuarios(0);
					}
				});
		  	}
		});
	}
	
	function validarUsuario(obj){
		if (obj.login.value.length == 0) {
				document.getElementById('errordial').innerHTML='El campo login no puede estar vacio.';
				$("#errordial").slideDown('slow')
				return false;
		}else{
			if(obj.pass.value != obj.rePass.value){
				document.getElementById('errordial').innerHTML='El password es diferente de Re-password.';
				$("#errordial").slideDown('slow')
				return false;
			}else
				return true;
		}
	}
	
	function agregaUsuario(obj){
		login = obj.login.value;
		pass = obj.pass.value;
		nombre = obj.nombre.value;
		email = obj.email.value;
		soloTablet = ( obj.soloTablet.checked ? '1' : '0' );
		$.ajax({
			type: "POST",
			url: 'funciones.php',
			data:'cmd=addUsuario&login=' + login + '&pass=' + pass + '&nombre=' + nombre + '&email=' + email + '&soloTablet=' + soloTablet,
			success: function(data){
				$('#cajas_menu_cont').html( data );	
			},
			beforeSend :function() {
		  		$('#cajas_menu_cont').html('<div style="width=100%" align="center"><img src="loader.gif" width="562" height="126" alt="Cargando" /></div>');
	  		}
		});
	}
	
	function editarUsuario(login){
		title = 'Editar Usuario';
		h = 500;
		w = 600;
		$.ajax({
			type: "POST",
		  	url: 'funciones.php',
		  	data:'cmd=editUsuario&loginUsuario=' + login,
		  	success: function ( data ){
					if ( data == -1 ){
						alert ( 'Su sesión a expirado' );
						window.location = 'logout.php';
					}else{
						$('.cajas_menu_cont').html(data);
					}
		  	},
		  	beforeSend :function() {
			  	actualizar = false;
				$("body").append ( '<div class="cat"><div class="cajas_menu_cont" id="cajas_menu_cont"><div style="margin-top:36px; witdh=90%" align="center" >' +
									'<img src="loader.gif" width="562" height="126" alt="Cargando" /></div></div></div>');
				$('.cat').dialog({ modal:true,
					closeText: 'Cerrar',
					title: title,
					height: h,
					width: w,
					close: function(){ //indicamos la función que se ejecutará al cerrarse la ventana
						$(this).remove(); //borramos la capa
						getUsuarios(0);
					}
				});
		  	}
		});
	}
	
	function editaUsuario(obj){
		login = obj.login.value;
		pass = obj.pass.value;
		nombre = obj.nombre.value;
		email = obj.email.value;
		soloTablet = ( obj.soloTablet.checked ? '1' : '0' );
		$.ajax({
			type: "POST",
			url: 'funciones.php',
			data:'cmd=editUsuario&login=' + login + '&nombre=' + nombre + '&pass=' + pass + '&email=' + email + '&soloTablet=' + soloTablet,
			success: function(data){
				$('#cajas_menu_cont').html( data );
				getUsuarios(0);
			},
			beforeSend :function() {
		  		$('#cajas_menu_cont').html('<div style="width=100%" align="center"><img src="loader.gif" width="562" height="126" alt="Cargando" /></div>');
	  		}
		});
	}
	
	function borrarUsuario(login){
		if (window.confirm('El registro será eliminado permanentemente. ¿Desea continuar?') == true){
			$.ajax({
				type: "POST",
				url: 'funciones.php',
				data:'cmd=delUsuario&loginUsuario=' + login,
				success: function(data){
					$('#cajas_menu_cont').html( data );
					getUsuarios(0);
				},
				beforeSend :function() {
					$('#cajas_menu_cont').html('<div style="width=100%" align="center"><img src="loader.gif" width="562" height="126" alt="Cargando" /></div>');
				}
			});
		}
	}
	
</script>
<body>
	<div id="container">
    	<table class="tPrincipal" border="0" cellspacing="0" cellpadding="0">
        	<tr id="trHeader">
        	  <td colspan="3">
            	<div class="barraSup1" style="" align="right">
                	<font color="#FFFF00"><?php echo $_SESSION['usrNombre']; ?></font> <a href='logout.php' class='link1'>Cerrar sesión</a>
                </div>
                <div class="barraSup2" align="center">
                    	<img src="img/titulo.png" style="margin-bottom:17px; margin-right:100px;"/>
                        <a href="inventario.php"><img src="img/inventario.png" style="margin-top:9px"/></a>
                        <a href="eventos.php"><img src="img/eventos.png" style="margin-top:9px"/></a>
                        <a href="usuarios.php"><img src="img/usuariosPress.png" style="margin-top:9px"/></a>
                </div>
            </td></tr>
            <tr id="trCuerpo"><td width="400px"><img src="img/fondoIzqCentro.png" /></td>
            	<td id="tdMainInfo">
                    <div class="mainInfo" align="center" style="height:auto">
                        <p style="text-align:left"><font size="+3">USUARIOS</font></p><div class="barraInferior"></div>
                        <div id="divDatosUsuarios">
                            <?php echo getUsuarios('0', '', '', '', '');?>
                        </div>
                    </div>
            	</td><td width="400px"><img src="img/fondoDerCentro.png" /></td></tr>
            <tr id="trPie"><td colspan="3">
                <div class="pie" style="vertical-align:bottom">
                  <p style="color:#FFF; margin-top:18px">&copy; Hersheys de Mexico 2014</p>
                </div>
            </td></tr>
        </table>
    </div>
</body>
</html>